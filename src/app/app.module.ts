import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

import { ContentbodyComponent } from './components/contentbody/contentbody.component';

import {DialogModule} from 'primeng/dialog';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import {GoogleLoginProvider, SocialAuthService} from 'angularx-social-login';

import { HttpClientModule } from '@angular/common/http';

import { LoginOverlayFormComponent } from './components/login-overlay-form/login-overlay-form.component';

import { NavbarComponent } from './components/navbar/navbar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';


import { ReactiveFormsModule } from '@angular//forms';

import { SocialAuthServiceConfig } from 'angularx-social-login';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ContentbodyComponent,
    LoginOverlayFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    NgbModule,
    DialogModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  providers: [{
    provide: 'SocialAuthServiceConfig',
    useValue: {

      autoLogin: true, //keeps the user signed in
      providers: [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider('116852492535-37n739s732ui71hkfm19n5r3agv6g9c5.apps.googleusercontent.com') // your client id
        }
      ]
    } as SocialAuthServiceConfig
  }, SocialAuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
