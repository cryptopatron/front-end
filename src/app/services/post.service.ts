import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Injectable } from '@angular/core';


@Injectable({
    providedIn: 'root'
})
export class PostService {

    private googleAuth = window.origin + '/auth/google/jwt';
    private url = '';

    constructor(private httpClient: HttpClient) { }

    get() {
        return this.httpClient.get(this.url);
    }

    postGoogleAuth(post: any) {
        console.log("post request to backend")
        const headers = new HttpHeaders()
            .set( 'content-type', 'application/json');
        return this.httpClient.post(this.googleAuth, JSON.stringify(post), {headers: headers});
    }

}
