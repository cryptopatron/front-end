import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'

@Component({
  selector: 'app-contentbody',
  templateUrl: './contentbody.component.html',
  styleUrls: ['./contentbody.component.sass']
})
export class ContentbodyComponent implements OnInit {

  name: string = '';
  creatorSearchFormCSS: boolean = true;

  constructor(
    private router: Router,
  ) {
    // do nothing.
  }

  ngOnInit(): void {
    // do nothing.
  }

  onCreatorSearch(): void {
    this.router.navigateByUrl('/creatorList');
  }

  onPageCreate(): void {
    this.router.navigateByUrl('/newpage');
  }


  creatorSearch: FormGroup = new FormGroup({
    creatorName: new FormControl('', [
      Validators.minLength(4),
      Validators.maxLength(14)
    ])
  });

  pageCreate: FormGroup = new FormGroup({
    pageName: new FormControl('',[
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(14)
    ])
  });

}
