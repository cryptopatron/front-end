import { Component, OnInit} from '@angular/core';
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {LoginOverlayFormComponent} from '../login-overlay-form/login-overlay-form.component';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass'],
})
export class NavbarComponent implements OnInit {
  
  //Menu Item icons
  faBars = faBars;
  faTimes = faTimes;
  
  // Value chooses menu item icon
  onToggle: number = 1;

  // Classes used for mobile menu toggle
  isActive = false;
  isMenu = true;

  // Reason for closing modal
  closeResult = '';

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
    this.setCurrentMenuToggleClasses();
  }

  //Toggle classes data structure
  toggleClasses: Record<string, boolean> = {};

  /*
   * initialize the toggle values
   */
   setCurrentMenuToggleClasses(){
    this.toggleClasses = {
      active: this.isActive,
      menu: this.isMenu
    };
  }

  /*
   * To toggle the menu for responsive design
   */
  toggleActive() {
    this.isActive = !this.isActive
    if(this.onToggle == 1)
      this.onToggle = 0;
    else
      this.onToggle = 1;
    this.setCurrentMenuToggleClasses();
  }

  /**
   * Run on click of buttons
   */
  onClick(action: string) {
    console.log('You have clicked ' + action);
  }

  open() {
    this.modalService.open(LoginOverlayFormComponent, {
        centered: true,
        size: 'lg',
        modalDialogClass: 'login-overlay',
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    console.log(this.closeResult);
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
