import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GoogleLoginProvider } from "angularx-social-login";

import { PostService } from "../../services/post.service"

import {Router} from '@angular/router';

import { SocialAuthService } from "angularx-social-login";
import { SocialUser } from "angularx-social-login";
import { Subscription } from 'rxjs';


@Component({
    selector: 'app-login-overlay-form',
    templateUrl: './login-overlay-form.component.html',
    styleUrls: ['./login-overlay-form.component.sass'],
    encapsulation: ViewEncapsulation.None,
})
export class LoginOverlayFormComponent implements OnInit, OnDestroy {

    constructor(public activeModal: NgbActiveModal, private authService: SocialAuthService, private postService: PostService, private router: Router) { }
    user?: SocialUser;
    loggedIn: boolean = false;
    private subscriptions: Subscription[] = [];

    ngOnInit(): void {
        console.log("ngonint is called")
        var subscription = this.authService.authState.subscribe((user) => {
            this.loggedIn = (user != null)
            if (this.loggedIn) {
                this.postService.postGoogleAuth({ idToken: user.idToken }).subscribe((res) => {
                    console.log(JSON.stringify(res))
                })
            }

        })
        this.subscriptions.push(subscription)
    }

    signInWithGoogle(): void {
        if (this.loggedIn === false) {
            this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(() => {
                this.router.navigate(['newpage'])
            });
        }
    }

    SignInWithMetamask(): void {
        console.log("Sign in with metamask")
    }

    signOut(): void {
        this.authService.signOut();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }


}
