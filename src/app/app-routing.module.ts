import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContentbodyComponent } from './components/contentbody/contentbody.component';

const routes: Routes = [
  {
    path: '',
    component: ContentbodyComponent
  },
  {
    path: 'creatorList',
    component: ContentbodyComponent
  },
  {
    path: 'newpage',
    component: ContentbodyComponent
  },
  {
    path:'**',
    component: ContentbodyComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
