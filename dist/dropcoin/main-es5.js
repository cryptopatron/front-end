(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (self["webpackChunkdropcoin"] = self["webpackChunkdropcoin"] || []).push([["main"], {
    /***/
    8255:
    /*!*******************************************************!*\
      !*** ./$_lazy_route_resources/ lazy namespace object ***!
      \*******************************************************/

    /***/
    function _(module) {
      function webpackEmptyAsyncContext(req) {
        // Here Promise.resolve().then() is used instead of new Promise() to prevent
        // uncaught exception popping up in devtools
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      webpackEmptyAsyncContext.keys = function () {
        return [];
      };

      webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
      webpackEmptyAsyncContext.id = 8255;
      module.exports = webpackEmptyAsyncContext;
      /***/
    },

    /***/
    158:
    /*!***************************************!*\
      !*** ./src/app/app-routing.module.ts ***!
      \***************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "AppRoutingModule": function AppRoutingModule() {
          return (
            /* binding */
            _AppRoutingModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      9895);
      /* harmony import */


      var _components_contentbody_contentbody_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./components/contentbody/contentbody.component */
      4331);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      7716);

      var routes = [{
        path: '',
        component: _components_contentbody_contentbody_component__WEBPACK_IMPORTED_MODULE_0__.ContentbodyComponent
      }, {
        path: 'creatorList',
        component: _components_contentbody_contentbody_component__WEBPACK_IMPORTED_MODULE_0__.ContentbodyComponent
      }, {
        path: 'newpage',
        component: _components_contentbody_contentbody_component__WEBPACK_IMPORTED_MODULE_0__.ContentbodyComponent
      }, {
        path: '**',
        component: _components_contentbody_contentbody_component__WEBPACK_IMPORTED_MODULE_0__.ContentbodyComponent
      }];

      var _AppRoutingModule = function _AppRoutingModule() {
        _classCallCheck(this, _AppRoutingModule);
      };

      _AppRoutingModule.ɵfac = function AppRoutingModule_Factory(t) {
        return new (t || _AppRoutingModule)();
      };

      _AppRoutingModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
        type: _AppRoutingModule
      });
      _AppRoutingModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](_AppRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule]
        });
      })();
      /***/

    },

    /***/
    5041:
    /*!**********************************!*\
      !*** ./src/app/app.component.ts ***!
      \**********************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "AppComponent": function AppComponent() {
          return (
            /* binding */
            _AppComponent
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      7716);
      /* harmony import */


      var _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./components/navbar/navbar.component */
      3252);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      9895);

      var _AppComponent = function _AppComponent() {
        _classCallCheck(this, _AppComponent);

        this.title = 'dropcoin';
      };

      _AppComponent.ɵfac = function AppComponent_Factory(t) {
        return new (t || _AppComponent)();
      };

      _AppComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: _AppComponent,
        selectors: [["app-root"]],
        decls: 4,
        vars: 0,
        consts: [[1, "top-container"], [1, "navbar-container"]],
        template: function AppComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "app-navbar");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "router-outlet");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
          }
        },
        directives: [_components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_0__.NavbarComponent, _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterOutlet],
        styles: [".top-container[_ngcontent-%COMP%] {\n  background-color: #F6F6F6;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQuc2FzcyIsIi4uL2Fzc2V0cy9zdHlsZXMvbGlnaHQtdGhlbWUtY29sb3JzLnNhc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDSSx5QkNIcUI7QURFekIiLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5zYXNzIiwic291cmNlc0NvbnRlbnQiOlsiQHVzZSAnLi4vYXNzZXRzL3N0eWxlcy9saWdodC10aGVtZS1jb2xvcnMnXG5cbi50b3AtY29udGFpbmVyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHQtdGhlbWUtY29sb3JzLiRsaWdodC1iYWNrZ3JvdW5kLWNvbG9yXG4iLCIkbGlnaHQtYmFja2dyb3VuZC1jb2xvcjogI0Y2RjZGNlxuJGxpZ2h0LXByaW1hcnktY29sb3I6ICM3OWQxYzNcbiRsaWdodC1zZWNvbmRhcnktY29sb3I6ICM2ODkyRDVcbiRsaWdodC1wcmltYXJ5LWZvbnQtY29sb3I6ICMwMDAwMDBcbiRsaWdodC1zZWNvbmRhcnktZm9udC1jb2xvcjogIzlEOUQ5RFxuJGxpZ2h0LXRlcnRpYXJ5LWNvbG9yOiAjZDRjMGZmXG4kbGlnaHQtcHJpbWFyeS1kYXJrLWNvbG9yOiAjNTc4MjdGXG4iXX0= */"]
      });
      /***/
    },

    /***/
    6747:
    /*!*******************************!*\
      !*** ./src/app/app.module.ts ***!
      \*******************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "AppModule": function AppModule() {
          return (
            /* binding */
            _AppModule
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var _app_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./app-routing.module */
      158);
      /* harmony import */


      var _app_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./app.component */
      5041);
      /* harmony import */


      var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @angular/platform-browser/animations */
      5835);
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/platform-browser */
      9075);
      /* harmony import */


      var _components_contentbody_contentbody_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./components/contentbody/contentbody.component */
      4331);
      /* harmony import */


      var primeng_dialog__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! primeng/dialog */
      2404);
      /* harmony import */


      var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @fortawesome/angular-fontawesome */
      4163);
      /* harmony import */


      var angularx_social_login__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! angularx-social-login */
      7055);
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! @angular/common/http */
      1841);
      /* harmony import */


      var _components_login_overlay_form_login_overlay_form_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./components/login-overlay-form/login-overlay-form.component */
      7725);
      /* harmony import */


      var _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./components/navbar/navbar.component */
      3252);
      /* harmony import */


      var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @ng-bootstrap/ng-bootstrap */
      2664);
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular//forms */
      3679);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/core */
      7716);

      var _AppModule = function _AppModule() {
        _classCallCheck(this, _AppModule);
      };

      _AppModule.ɵfac = function AppModule_Factory(t) {
        return new (t || _AppModule)();
      };

      _AppModule.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({
        type: _AppModule,
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_1__.AppComponent]
      });
      _AppModule.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({
        providers: [{
          provide: 'SocialAuthServiceConfig',
          useValue: {
            autoLogin: true,
            providers: [{
              id: angularx_social_login__WEBPACK_IMPORTED_MODULE_6__.GoogleLoginProvider.PROVIDER_ID,
              provider: new angularx_social_login__WEBPACK_IMPORTED_MODULE_6__.GoogleLoginProvider('116852492535-37n739s732ui71hkfm19n5r3agv6g9c5.apps.googleusercontent.com') // your client id

            }]
          }
        }, angularx_social_login__WEBPACK_IMPORTED_MODULE_6__.SocialAuthService],
        imports: [[_angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__.BrowserModule, _app_routing_module__WEBPACK_IMPORTED_MODULE_0__.AppRoutingModule, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.ReactiveFormsModule, _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_9__.FontAwesomeModule, _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_10__.NgbModule, primeng_dialog__WEBPACK_IMPORTED_MODULE_11__.DialogModule, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__.BrowserAnimationsModule, _angular_common_http__WEBPACK_IMPORTED_MODULE_13__.HttpClientModule]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵsetNgModuleScope"](_AppModule, {
          declarations: [_app_component__WEBPACK_IMPORTED_MODULE_1__.AppComponent, _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_4__.NavbarComponent, _components_contentbody_contentbody_component__WEBPACK_IMPORTED_MODULE_2__.ContentbodyComponent, _components_login_overlay_form_login_overlay_form_component__WEBPACK_IMPORTED_MODULE_3__.LoginOverlayFormComponent],
          imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__.BrowserModule, _app_routing_module__WEBPACK_IMPORTED_MODULE_0__.AppRoutingModule, _angular_forms__WEBPACK_IMPORTED_MODULE_8__.ReactiveFormsModule, _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_9__.FontAwesomeModule, _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_10__.NgbModule, primeng_dialog__WEBPACK_IMPORTED_MODULE_11__.DialogModule, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__.BrowserAnimationsModule, _angular_common_http__WEBPACK_IMPORTED_MODULE_13__.HttpClientModule]
        });
      })();
      /***/

    },

    /***/
    4331:
    /*!*****************************************************************!*\
      !*** ./src/app/components/contentbody/contentbody.component.ts ***!
      \*****************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "ContentbodyComponent": function ContentbodyComponent() {
          return (
            /* binding */
            _ContentbodyComponent
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular//forms */
      3679);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      7716);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      9895);

      var _ContentbodyComponent = /*#__PURE__*/function () {
        function _ContentbodyComponent(router) {
          _classCallCheck(this, _ContentbodyComponent);

          this.router = router;
          this.name = '';
          this.creatorSearchFormCSS = true;
          this.creatorSearch = new _angular_forms__WEBPACK_IMPORTED_MODULE_0__.FormGroup({
            creatorName: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__.FormControl('', [_angular_forms__WEBPACK_IMPORTED_MODULE_0__.Validators.minLength(4), _angular_forms__WEBPACK_IMPORTED_MODULE_0__.Validators.maxLength(14)])
          });
          this.pageCreate = new _angular_forms__WEBPACK_IMPORTED_MODULE_0__.FormGroup({
            pageName: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__.FormControl('', [_angular_forms__WEBPACK_IMPORTED_MODULE_0__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_0__.Validators.minLength(3), _angular_forms__WEBPACK_IMPORTED_MODULE_0__.Validators.maxLength(14)])
          }); // do nothing.
        }

        _createClass(_ContentbodyComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {// do nothing.
          }
        }, {
          key: "onCreatorSearch",
          value: function onCreatorSearch() {
            this.router.navigateByUrl('/creatorList');
          }
        }, {
          key: "onPageCreate",
          value: function onPageCreate() {
            this.router.navigateByUrl('/newpage');
          }
        }]);

        return _ContentbodyComponent;
      }();

      _ContentbodyComponent.ɵfac = function ContentbodyComponent_Factory(t) {
        return new (t || _ContentbodyComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__.Router));
      };

      _ContentbodyComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: _ContentbodyComponent,
        selectors: [["app-contentbody"]],
        decls: 36,
        vars: 6,
        consts: [[1, "content-panel"], ["id", "journey-start", 1, "center-block"], [1, "make-a-page-text"], ["id", "jiffy", 1, "content-heading"], [1, "info-text"], [3, "formGroup", "ngSubmit"], ["id", "creatorPageName", 1, "enterName"], ["id", "startPageCreate", "type", "text", "formControlName", "pageName", "placeholder", "pagename"], ["id", "startPageCreation", 1, "button"], ["type", "submit", 1, "button"], [1, "center-block"], [1, "info-text", "text-left"], [1, "man-playing-games", "place-right"], ["src", "../../../assets/images/man-playing-video-game.svg"], [1, "search-creator"], [1, "content-heading", "text-left"], ["id", "patron-call", 1, "info-text"], ["id", "creator", 1, "search-bar"], ["id", "creatorFind", "type", "text", "formControlName", "creatorName", "placeholder", "Find Creator"], ["id", "creatorFindSubmit", 1, "button"], ["id", "woman-section", 1, "center-block"], [1, "info-text", "text-right"], [1, "woman-on-computer", "place-left"], ["src", "../../../assets/images/the-modern-woman.svg"], [1, "footer"]],
        template: function ContentbodyComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "h4", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Create your page in a jiffy.");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "p", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Its absolutely free. No gas fees, No extra charges");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "form", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngSubmit", function ContentbodyComponent_Template_form_ngSubmit_7_listener() {
              return ctx.onPageCreate();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](9, "input", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "button", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12, "Create");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "p", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15, "K\u014Den allows your patrons to support you in a modern, secure approach. Diversify your commodities and make a move outside the contemporary. Crypto is and will be and a great investment.");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "div", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](17, "img", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "div", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "h2", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](21, "Like a creator?");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "h2", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](23, "Support them!");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "form", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngSubmit", function ContentbodyComponent_Template_form_ngSubmit_24_listener() {
              return ctx.onCreatorSearch();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](26, "input", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "button", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](29, "Search");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](30, "div", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](31, "p", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](32, "Find your own space! Share your views to the world and unite the people. Have a chill zone with your community. Put your merch up for grabs and create a new beginning");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](33, "div", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](34, "img", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](35, "div", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassProp"]("pageCreateForm", ctx.creatorSearchFormCSS);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formGroup", ctx.pageCreate);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](17);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassProp"]("creatorSearchForm", ctx.creatorSearchFormCSS);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formGroup", ctx.creatorSearch);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_0__.FormGroupDirective, _angular_forms__WEBPACK_IMPORTED_MODULE_0__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_0__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_0__.FormControlName],
        styles: ["@media all and (max-width: 699px) {\n  .content-panel[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    padding: 2.5em;\n    padding-top: 0em;\n    flex: 1;\n    flex-flow: wrap;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    justify-content: space-evenly;\n    padding-right: 0em;\n    height: 300px;\n    padding: 1em;\n    flex: 1;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%]   .make-a-page-text[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    width: 240px;\n    align-self: center;\n    text-align: center;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%]   .make-a-page-text[_ngcontent-%COMP%]   #jiffy[_ngcontent-%COMP%] {\n    margin-bottom: 0em;\n    font-family: poppins, sans-serif;\n    font-size: 26px;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%]   .make-a-page-text[_ngcontent-%COMP%]   .info-text[_ngcontent-%COMP%] {\n    font-family: poppins, sans-serif;\n    font-size: 20px;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%]   .pageCreateForm[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    flex: 1;\n    align-items: center;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%]   .pageCreateForm[_ngcontent-%COMP%]   #creatorPageName[_ngcontent-%COMP%]   #startPageCreate[_ngcontent-%COMP%] {\n    border-radius: 20px;\n    border: none;\n    height: 40px;\n    width: 300px;\n    font-size: 20px;\n    font-weight: 700;\n    background-color: #e8e8e8;\n    text-align: center;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%]   .pageCreateForm[_ngcontent-%COMP%]   #startPageCreation[_ngcontent-%COMP%] {\n    margin-top: 20px;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%]   .pageCreateForm[_ngcontent-%COMP%]   #startPageCreation[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%] {\n    height: 40px;\n    width: 300px;\n    border: none;\n    border-radius: 20px;\n    color: #6892D5;\n    background-color: #79D1C3;\n    font-weight: 700;\n    font-size: 16px;\n    transition: 0.3s;\n  }\n  .content-panel[_ngcontent-%COMP%]   .center-block[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    padding: 1em;\n    height: 440px;\n    width: inherit;\n    flex: 1;\n    align-items: center;\n  }\n  .content-panel[_ngcontent-%COMP%]   .center-block[_ngcontent-%COMP%]   .info-text.text-left[_ngcontent-%COMP%] {\n    font-family: poppins, sans-serif;\n    width: 250px;\n    font-weight: 500;\n    font-size: 16px;\n    padding-top: 50px;\n  }\n  .content-panel[_ngcontent-%COMP%]   .center-block[_ngcontent-%COMP%]   .man-playing-games[_ngcontent-%COMP%] {\n    height: 140px;\n  }\n  .content-panel[_ngcontent-%COMP%]   .center-block[_ngcontent-%COMP%]   .man-playing-games[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    height: 120px;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    justify-content: space-evenly;\n    padding-right: 0em;\n    height: 270px;\n    padding: 1em;\n    flex: 1;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .content-heading.text-left[_ngcontent-%COMP%] {\n    align-self: center;\n    font-family: poppins, sans-serif;\n    margin-bottom: 20px;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .creatorSearchForm[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    flex: 1;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .creatorSearchForm[_ngcontent-%COMP%]   #creator[_ngcontent-%COMP%]   #creatorFind[_ngcontent-%COMP%] {\n    border-radius: 20px;\n    border: none;\n    height: 40px;\n    width: 300px;\n    font-size: 20px;\n    font-weight: 700;\n    background-color: #e8e8e8;\n    text-align: center;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .creatorSearchForm[_ngcontent-%COMP%]   #creatorFindSubmit[_ngcontent-%COMP%] {\n    padding-top: 20px;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .creatorSearchForm[_ngcontent-%COMP%]   #creatorFindSubmit[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%] {\n    height: 40px;\n    width: 300px;\n    border: none;\n    border-radius: 20px;\n    color: #6892D5;\n    background-color: #79D1C3;\n    font-weight: 700;\n    font-size: 16px;\n    transition: 0.3s;\n  }\n  .content-panel[_ngcontent-%COMP%]   #woman-section[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column-reverse;\n    padding: 0em;\n    align-items: center;\n    justify-content: center;\n    flex-flow: wrap-reverse;\n  }\n  .content-panel[_ngcontent-%COMP%]   #woman-section[_ngcontent-%COMP%]   .info-text.text-right[_ngcontent-%COMP%] {\n    padding-right: 0px;\n    font-size: 15px;\n    font-weight: 500;\n    font-family: poppins, sans-serif;\n    padding: 1em;\n  }\n  .content-panel[_ngcontent-%COMP%]   #woman-section[_ngcontent-%COMP%]   .woman-on-computer[_ngcontent-%COMP%] {\n    height: 120px;\n    width: 260px;\n    padding-bottom: 100px;\n    margin-bottom: 100px;\n  }\n  .content-panel[_ngcontent-%COMP%]   #woman-section[_ngcontent-%COMP%]   .woman-on-computer[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    height: 290px;\n  }\n}\n\n@media all and (min-width: 700px) and (max-width: 959px) {\n  .content-panel[_ngcontent-%COMP%] {\n    padding: 3em;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%] {\n    justify-content: space-between;\n    display: flex;\n    flex-direction: row;\n    flex-wrap: wrap;\n    flex: 1;\n    height: 260px;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%]   .make-a-page-text[_ngcontent-%COMP%] {\n    width: 200px;\n    display: flex;\n    flex-direction: column;\n    padding-left: 3em;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%]   .make-a-page-text[_ngcontent-%COMP%]   #jiffy[_ngcontent-%COMP%] {\n    font-size: 26px;\n    margin-bottom: 0em;\n    font-family: poppins, sans-serif;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%]   .make-a-page-text[_ngcontent-%COMP%]   .info-text[_ngcontent-%COMP%] {\n    font-family: poppins, sans-serif;\n    font-size: 20px;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%]   .pageCreateForm[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row;\n    align-items: center;\n    padding-right: 3em;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%]   .pageCreateForm[_ngcontent-%COMP%]   #creatorPageName[_ngcontent-%COMP%]   #startPageCreate[_ngcontent-%COMP%] {\n    height: 40px;\n    border: none;\n    border-radius: 20px;\n    padding: 0px 30px;\n    width: 250px;\n    font-size: 20px;\n    font-weight: 700;\n    background-color: #e8e8e8;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%]   .pageCreateForm[_ngcontent-%COMP%]   #startPageCreation[_ngcontent-%COMP%] {\n    margin-left: 20px;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%]   .pageCreateForm[_ngcontent-%COMP%]   #startPageCreation[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%] {\n    border: none;\n    height: 40px;\n    width: 100px;\n    border-radius: 20px;\n    color: #6892D5;\n    background-color: #79D1C3;\n    font-weight: 700;\n    font-size: 16px;\n    transition: 0.3s;\n  }\n  .content-panel[_ngcontent-%COMP%]   #journey-start[_ngcontent-%COMP%]   .pageCreateForm[_ngcontent-%COMP%]   #startPageCreation[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%]:hover {\n    box-shadow: 3px 3px 3px;\n  }\n  .content-panel[_ngcontent-%COMP%]   .center-block[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row;\n    justify-content: space-around;\n    height: -webkit-fit-content;\n    height: -moz-fit-content;\n    height: fit-content;\n    padding-top: 1em;\n  }\n  .content-panel[_ngcontent-%COMP%]   .center-block[_ngcontent-%COMP%]   .info-text.text-left[_ngcontent-%COMP%] {\n    font-size: 16px;\n    line-height: 25px;\n    width: 200px;\n    font-family: poppins, sans-serif;\n    align-self: center;\n    font-weight: 500;\n  }\n  .content-panel[_ngcontent-%COMP%]   .center-block[_ngcontent-%COMP%]   .man-playing-games[_ngcontent-%COMP%] {\n    height: 250px;\n    align-self: center;\n  }\n  .content-panel[_ngcontent-%COMP%]   .center-block[_ngcontent-%COMP%]   .man-playing-games[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    height: 250px;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%] {\n    justify-content: space-between;\n    display: flex;\n    flex-direction: row;\n    flex-wrap: wrap;\n    flex: 1;\n    height: 140px;\n    align-items: center;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .content-heading.text-left[_ngcontent-%COMP%] {\n    width: 200px;\n    display: flex;\n    flex-direction: column;\n    padding-left: 3em;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .content-heading.text-left[_ngcontent-%COMP%]   #patron-call[_ngcontent-%COMP%] {\n    margin-bottom: 0px;\n    margin-top: 5px;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .creatorSearchForm[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row;\n    align-items: center;\n    padding-right: 3em;\n    height: -webkit-fit-content;\n    height: -moz-fit-content;\n    height: fit-content;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .creatorSearchForm[_ngcontent-%COMP%]   #creator[_ngcontent-%COMP%]   #creatorFind[_ngcontent-%COMP%] {\n    height: 40px;\n    border: none;\n    border-radius: 20px;\n    padding: 0px 30px;\n    width: 250px;\n    font-size: 20px;\n    font-weight: 700;\n    background-color: #e8e8e8;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .creatorSearchForm[_ngcontent-%COMP%]   #creatorFindSubmit[_ngcontent-%COMP%] {\n    margin-left: 20px;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .creatorSearchForm[_ngcontent-%COMP%]   #creatorFindSubmit[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%] {\n    border: none;\n    height: 40px;\n    width: 100px;\n    border-radius: 20px;\n    color: #6892D5;\n    background-color: #79D1C3;\n    font-weight: 700;\n    font-size: 16px;\n    transition: 0.3s;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .creatorSearchForm[_ngcontent-%COMP%]   #creatorFindSubmit[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%]:hover {\n    box-shadow: 3px 3px 3px;\n  }\n  .content-panel[_ngcontent-%COMP%]   #woman-section[_ngcontent-%COMP%] {\n    align-items: center;\n    flex-direction: row-reverse;\n    display: flex;\n    padding: 1em;\n    justify-content: space-between;\n  }\n  .content-panel[_ngcontent-%COMP%]   #woman-section[_ngcontent-%COMP%]   .info-text.text-right[_ngcontent-%COMP%] {\n    width: 220px;\n    padding-right: 1em;\n    font-family: poppins, sans-serif;\n    font-size: 16px;\n    font-weight: 500;\n  }\n  .content-panel[_ngcontent-%COMP%]   #woman-section[_ngcontent-%COMP%]   .woman-on-computer[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    height: 450px;\n  }\n}\n\n@media all and (min-width: 960px) {\n  #pagename-input[_ngcontent-%COMP%] {\n    border: 1px solid transparent;\n    font-family: \"graphik 400\", sans-serif;\n    font-size: 24px;\n    line-height: 65px;\n    color: #0d0c22;\n    width: 155px;\n    background: 0 0;\n    caret-color: #0d0c22;\n    text-align: left;\n    margin-left: 1px;\n  }\n\n  .content-panel[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    padding: 4.5em;\n    flex: 1 0;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row;\n    justify-content: space-evenly;\n    padding-right: 0em;\n    height: 100px;\n    padding: 2em;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .content-heading.text-left[_ngcontent-%COMP%] {\n    align-self: center;\n    font-family: poppins, sans-serif;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .content-heading.text-left[_ngcontent-%COMP%]   #patron-call[_ngcontent-%COMP%] {\n    margin-top: 0px;\n    margin-bottom: 0px;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .creatorSearchForm[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row;\n    align-items: center;\n  }\n  .content-panel[_ngcontent-%COMP%]   .search-creator[_ngcontent-%COMP%]   .creatorSearchForm[_ngcontent-%COMP%]   #creator[_ngcontent-%COMP%]   input[type=text][_ngcontent-%COMP%] {\n    padding: 0 30px;\n  }\n\n  #creatorFind[_ngcontent-%COMP%] {\n    height: 50px;\n    width: 420px;\n    border: none;\n    border-radius: 20px;\n    font-size: 20px;\n    font-weight: 700;\n    background-color: #e8e8e8;\n  }\n\n  #creatorFind[_ngcontent-%COMP%]::-moz-placeholder {\n    font-size: 15px !important;\n    font-weight: 700 !important;\n    opacity: 0.42 !important;\n    color: #000 !important;\n  }\n\n  #creatorFind[_ngcontent-%COMP%]:-ms-input-placeholder {\n    font-size: 15px !important;\n    font-weight: 700 !important;\n    opacity: 0.42 !important;\n    color: #000 !important;\n  }\n\n  #creatorFind[_ngcontent-%COMP%]::placeholder {\n    font-size: 15px !important;\n    font-weight: 700 !important;\n    opacity: 0.42 !important;\n    color: #000 !important;\n  }\n\n  #creatorFind[_ngcontent-%COMP%]:focus {\n    outline: none;\n  }\n\n  #creatorFindSubmit[_ngcontent-%COMP%] {\n    margin-left: 20px;\n  }\n  #creatorFindSubmit[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%] {\n    height: 50px;\n    width: 150px;\n    border: none;\n    border-radius: 20px;\n    font-size: 20px;\n    font-weight: 700;\n    color: #6892D5;\n    background-color: #79D1C3;\n    transition: 0.3s;\n  }\n  #creatorFindSubmit[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%]:hover {\n    box-shadow: 3px 3px 3px;\n  }\n\n  .center-block[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row;\n    justify-content: space-around;\n    padding: 1em;\n    height: 500px;\n  }\n  .center-block[_ngcontent-%COMP%]   .info-text.text-left[_ngcontent-%COMP%] {\n    font-family: poppins, sans-serif;\n    width: 260px;\n    align-self: center;\n    line-height: 25px;\n    font-weight: 500;\n    font-size: 20px;\n    padding-left: 60px;\n  }\n  .center-block[_ngcontent-%COMP%]   .man-playing-games.place-right[_ngcontent-%COMP%] {\n    height: 500px;\n    width: 600px;\n  }\n  .center-block[_ngcontent-%COMP%]   .man-playing-games.place-right[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    height: 450px;\n  }\n\n  #woman-section[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row-reverse;\n    padding: 2em;\n    align-items: center;\n    justify-content: space-between;\n  }\n  #woman-section[_ngcontent-%COMP%]   .info-text.text-right[_ngcontent-%COMP%] {\n    width: 220px;\n    padding-right: 140px;\n    font-size: 16px;\n    font-weight: 500;\n    font-family: poppins;\n  }\n  #woman-section[_ngcontent-%COMP%]   .woman-on-computer[_ngcontent-%COMP%] {\n    height: 700px;\n    width: 700px;\n  }\n  #woman-section[_ngcontent-%COMP%]   .woman-on-computer[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    height: 700px;\n  }\n\n  #journey-start[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row;\n    justify-content: space-evenly;\n    padding-right: 0em;\n    height: 100px;\n    padding: 2em;\n  }\n  #journey-start[_ngcontent-%COMP%]   .make-a-page-text[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    width: 200px;\n  }\n  #journey-start[_ngcontent-%COMP%]   .make-a-page-text[_ngcontent-%COMP%]   #jiffy[_ngcontent-%COMP%] {\n    margin-bottom: 0px;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    font-family: \"poppins\", sans-serif;\n    font-size: 20px;\n    line-height: 25px;\n  }\n  #journey-start[_ngcontent-%COMP%]   .make-a-page-text[_ngcontent-%COMP%]   .info-text[_ngcontent-%COMP%] {\n    font-family: poppins;\n    color: #A8A8A8;\n    font-weight: 700;\n  }\n  #journey-start[_ngcontent-%COMP%]   .pageCreateForm[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row;\n    align-items: center;\n  }\n  #journey-start[_ngcontent-%COMP%]   .pageCreateForm[_ngcontent-%COMP%]   #creatorPageName[_ngcontent-%COMP%]   input[type=text][_ngcontent-%COMP%] {\n    padding: 0 30px;\n  }\n\n  #startPageCreate[_ngcontent-%COMP%] {\n    height: 50px;\n    width: 420px;\n    border: none;\n    border-radius: 20px;\n    font-size: 20px;\n    font-weight: 700;\n    background-color: #e8e8e8;\n  }\n\n  #startPageCreate[_ngcontent-%COMP%]::-moz-placeholder {\n    font-size: 15px !important;\n    font-weight: 700 !important;\n    opacity: 0.42 !important;\n    color: #000 !important;\n  }\n\n  #startPageCreate[_ngcontent-%COMP%]:-ms-input-placeholder {\n    font-size: 15px !important;\n    font-weight: 700 !important;\n    opacity: 0.42 !important;\n    color: #000 !important;\n  }\n\n  #startPageCreate[_ngcontent-%COMP%]::placeholder {\n    font-size: 15px !important;\n    font-weight: 700 !important;\n    opacity: 0.42 !important;\n    color: #000 !important;\n  }\n\n  #startPageCreate[_ngcontent-%COMP%]:focus {\n    outline: none;\n  }\n\n  #startPageCreation[_ngcontent-%COMP%] {\n    margin-left: 20px;\n  }\n  #startPageCreation[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%] {\n    height: 50px;\n    width: 150px;\n    border: none;\n    border-radius: 20px;\n    font-size: 16px;\n    font-weight: 700;\n    color: #6892D5;\n    background-color: #79D1C3;\n    transition: 0.3s;\n  }\n  #startPageCreation[_ngcontent-%COMP%]   .button[_ngcontent-%COMP%]:hover {\n    box-shadow: 3px 3px 3px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbnRlbnRib2R5LmNvbXBvbmVudC5zYXNzIiwiLi4vLi4vLi4vYXNzZXRzL3N0eWxlcy9mb250cy5zYXNzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLFVBQUE7QUFDQTtFQUNJO0lBQ0ksYUFBQTtJQUNBLHNCQUFBO0lBQ0EsY0FBQTtJQUNBLGdCQUFBO0lBQ0EsT0FBQTtJQUNBLGVBQUE7RUFETjtFQUVNO0lBQ0ksYUFBQTtJQUNBLHNCQUFBO0lBQ0EsNkJBQUE7SUFDQSxrQkFBQTtJQUNBLGFBQUE7SUFDQSxZQUFBO0lBQ0EsT0FBQTtFQUFWO0VBQ1U7SUFDSSxhQUFBO0lBQ0Esc0JBQUE7SUFDQSxZQUFBO0lBQ0Esa0JBQUE7SUFDQSxrQkFBQTtFQUNkO0VBQWM7SUFDSSxrQkFBQTtJQUNBLGdDQUFBO0lBQ0EsZUFBQTtFQUVsQjtFQURjO0lBQ0ksZ0NBQUE7SUFDQSxlQUFBO0VBR2xCO0VBRlU7SUFDSSxhQUFBO0lBQ0Esc0JBQUE7SUFDQSxPQUFBO0lBQ0EsbUJBQUE7RUFJZDtFQUZrQjtJQUNJLG1CQUFBO0lBQ0EsWUFBQTtJQUNBLFlBQUE7SUFDQSxZQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0EseUJBQUE7SUFDQSxrQkFBQTtFQUl0QjtFQUhjO0lBQ0ksZ0JBQUE7RUFLbEI7RUFKa0I7SUFDSSxZQUFBO0lBQ0EsWUFBQTtJQUNBLFlBQUE7SUFDQSxtQkFBQTtJQUNBLGNBQUE7SUFDQSx5QkFBQTtJQUNBLGdCQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0VBTXRCO0VBTE07SUFDSSxhQUFBO0lBQ0Esc0JBQUE7SUFDQSx1QkFBQTtJQUNBLFlBQUE7SUFDQSxhQUFBO0lBQ0EsY0FBQTtJQUNBLE9BQUE7SUFDQSxtQkFBQTtFQU9WO0VBTlU7SUFDSSxnQ0FBQTtJQUNBLFlBQUE7SUFDQSxnQkFBQTtJQUNBLGVBQUE7SUFDQSxpQkFBQTtFQVFkO0VBUFU7SUFDSSxhQUFBO0VBU2Q7RUFSYztJQUNJLGFBQUE7RUFVbEI7RUFUTTtJQUNJLGFBQUE7SUFDQSxzQkFBQTtJQUNBLDZCQUFBO0lBQ0Esa0JBQUE7SUFDQSxhQUFBO0lBQ0EsWUFBQTtJQUNBLE9BQUE7RUFXVjtFQVZVO0lBQ0ksa0JBQUE7SUFDQSxnQ0FBQTtJQUNBLG1CQUFBO0VBWWQ7RUFYVTtJQUNJLGFBQUE7SUFDQSxzQkFBQTtJQUNBLG1CQUFBO0lBQ0EsT0FBQTtFQWFkO0VBWGtCO0lBQ0ksbUJBQUE7SUFDQSxZQUFBO0lBQ0EsWUFBQTtJQUNBLFlBQUE7SUFDQSxlQUFBO0lBQ0EsZ0JBQUE7SUFDQSx5QkFBQTtJQUNBLGtCQUFBO0VBYXRCO0VBWmM7SUFDSSxpQkFBQTtFQWNsQjtFQWJrQjtJQUNJLFlBQUE7SUFDQSxZQUFBO0lBQ0EsWUFBQTtJQUNBLG1CQUFBO0lBQ0EsY0FBQTtJQUNBLHlCQUFBO0lBQ0EsZ0JBQUE7SUFDQSxlQUFBO0lBQ0EsZ0JBQUE7RUFldEI7RUFkTTtJQUNJLGFBQUE7SUFDQSw4QkFBQTtJQUNBLFlBQUE7SUFDQSxtQkFBQTtJQUNBLHVCQUFBO0lBQ0EsdUJBQUE7RUFnQlY7RUFmVTtJQUNJLGtCQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0EsZ0NBQUE7SUFDQSxZQUFBO0VBaUJkO0VBZlU7SUFDSSxhQUFBO0lBQ0EsWUFBQTtJQUNBLHFCQUFBO0lBQ0Esb0JBQUE7RUFpQmQ7RUFoQmM7SUFDSSxhQUFBO0VBa0JsQjtBQUNGO0FBakJBLFdBQUE7QUFDQTtFQUNJO0lBQ0ksWUFBQTtFQW1CTjtFQWxCTTtJQUNJLDhCQUFBO0lBQ0EsYUFBQTtJQUNBLG1CQUFBO0lBQ0EsZUFBQTtJQUNBLE9BQUE7SUFDQSxhQUFBO0VBb0JWO0VBbkJVO0lBQ0ksWUFBQTtJQUNBLGFBQUE7SUFDQSxzQkFBQTtJQUNBLGlCQUFBO0VBcUJkO0VBcEJjO0lBQ0ksZUFBQTtJQUNBLGtCQUFBO0lBQ0EsZ0NBQUE7RUFzQmxCO0VBckJjO0lBQ0ksZ0NBQUE7SUFDQSxlQUFBO0VBdUJsQjtFQXRCVTtJQUNRLGFBQUE7SUFDQSxtQkFBQTtJQUNBLG1CQUFBO0lBQ0Esa0JBQUE7RUF3QmxCO0VBdEJzQjtJQUNJLFlBQUE7SUFDQSxZQUFBO0lBQ0EsbUJBQUE7SUFDQSxpQkFBQTtJQUNBLFlBQUE7SUFDQSxlQUFBO0lBQ0EsZ0JBQUE7SUFDQSx5QkFBQTtFQXdCMUI7RUF2QmtCO0lBQ0ksaUJBQUE7RUF5QnRCO0VBeEJzQjtJQUNJLFlBQUE7SUFDQSxZQUFBO0lBQ0EsWUFBQTtJQUNBLG1CQUFBO0lBQ0EsY0FBQTtJQUNBLHlCQUFBO0lBQ0EsZ0JBQUE7SUFDQSxlQUFBO0lBQ0EsZ0JBQUE7RUEwQjFCO0VBekIwQjtJQUNJLHVCQUFBO0VBMkI5QjtFQTFCTTtJQUNJLGFBQUE7SUFDQSxtQkFBQTtJQUNBLDZCQUFBO0lBQ0EsMkJBQUE7SUFBQSx3QkFBQTtJQUFBLG1CQUFBO0lBQ0EsZ0JBQUE7RUE0QlY7RUEzQlU7SUFDSSxlQUFBO0lBQ0EsaUJBQUE7SUFDQSxZQUFBO0lBQ0EsZ0NBQUE7SUFDQSxrQkFBQTtJQUNBLGdCQUFBO0VBNkJkO0VBNUJVO0lBQ0ksYUFBQTtJQUNBLGtCQUFBO0VBOEJkO0VBN0JjO0lBQ0ksYUFBQTtFQStCbEI7RUE5Qk07SUFDSSw4QkFBQTtJQUNBLGFBQUE7SUFDQSxtQkFBQTtJQUNBLGVBQUE7SUFDQSxPQUFBO0lBQ0EsYUFBQTtJQUNBLG1CQUFBO0VBZ0NWO0VBL0JVO0lBQ0ksWUFBQTtJQUNBLGFBQUE7SUFDQSxzQkFBQTtJQUNBLGlCQUFBO0VBaUNkO0VBaENjO0lBQ0ksa0JBQUE7SUFDQSxlQUFBO0VBa0NsQjtFQWpDVTtJQUNJLGFBQUE7SUFDQSxtQkFBQTtJQUNBLG1CQUFBO0lBQ0Esa0JBQUE7SUFDQSwyQkFBQTtJQUFBLHdCQUFBO0lBQUEsbUJBQUE7RUFtQ2Q7RUFqQ2tCO0lBQ0ksWUFBQTtJQUNBLFlBQUE7SUFDQSxtQkFBQTtJQUNBLGlCQUFBO0lBQ0EsWUFBQTtJQUNBLGVBQUE7SUFDQSxnQkFBQTtJQUNBLHlCQUFBO0VBbUN0QjtFQWxDYztJQUNJLGlCQUFBO0VBb0NsQjtFQW5Da0I7SUFDSSxZQUFBO0lBQ0EsWUFBQTtJQUNBLFlBQUE7SUFDQSxtQkFBQTtJQUNBLGNBQUE7SUFDQSx5QkFBQTtJQUNBLGdCQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0VBcUN0QjtFQXBDc0I7SUFDSSx1QkFBQTtFQXNDMUI7RUFyQ007SUFDSSxtQkFBQTtJQUNBLDJCQUFBO0lBQ0EsYUFBQTtJQUNBLFlBQUE7SUFDQSw4QkFBQTtFQXVDVjtFQXRDVTtJQUNJLFlBQUE7SUFDQSxrQkFBQTtJQUNBLGdDQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0VBd0NkO0VBdENjO0lBQ0ksYUFBQTtFQXdDbEI7QUFDRjtBQXZDQSxZQUFBO0FBQ0E7RUFDSTtJQUNJLDZCQUFBO0lBQ0Esc0NBQUE7SUFDQSxlQUFBO0lBQ0EsaUJBQUE7SUFDQSxjQUFBO0lBQ0EsWUFBQTtJQUNBLGVBQUE7SUFDQSxvQkFBQTtJQUNBLGdCQUFBO0lBQ0EsZ0JBQUE7RUF5Q047O0VBdkNFO0lBQ0ksYUFBQTtJQUNBLHNCQUFBO0lBQ0EsY0FBQTtJQUNBLFNBQUE7RUEwQ047RUF6Q007SUFDSSxhQUFBO0lBQ0EsbUJBQUE7SUFDQSw2QkFBQTtJQUNBLGtCQUFBO0lBQ0EsYUFBQTtJQUNBLFlBQUE7RUEyQ1Y7RUExQ1U7SUFDSSxrQkFBQTtJQUNBLGdDQUFBO0VBNENkO0VBM0NjO0lBQ0ksZUFBQTtJQUNBLGtCQUFBO0VBNkNsQjtFQTVDVTtJQUNJLGFBQUE7SUFDQSxtQkFBQTtJQUNBLG1CQUFBO0VBOENkO0VBNUNrQjtJQUNJLGVBQUE7RUE4Q3RCOztFQTFDRTtJQUNJLFlBQUE7SUFDQSxZQUFBO0lBQ0EsWUFBQTtJQUNBLG1CQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0EseUJBQUE7RUE2Q047O0VBM0NFO0lBQ0ksMEJBQUE7SUFDQSwyQkFBQTtJQUNBLHdCQUFBO0lBQ0Esc0JBQUE7RUE4Q047O0VBbERFO0lBQ0ksMEJBQUE7SUFDQSwyQkFBQTtJQUNBLHdCQUFBO0lBQ0Esc0JBQUE7RUE4Q047O0VBbERFO0lBQ0ksMEJBQUE7SUFDQSwyQkFBQTtJQUNBLHdCQUFBO0lBQ0Esc0JBQUE7RUE4Q047O0VBNUNFO0lBQ0ksYUFBQTtFQStDTjs7RUE3Q0U7SUFDSSxpQkFBQTtFQWdETjtFQS9DTTtJQUNJLFlBQUE7SUFDQSxZQUFBO0lBQ0EsWUFBQTtJQUNBLG1CQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0EsY0FBQTtJQUNBLHlCQUFBO0lBQ0EsZ0JBQUE7RUFpRFY7RUFoRE07SUFDSSx1QkFBQTtFQWtEVjs7RUEvQ0U7SUFDSSxhQUFBO0lBQ0EsbUJBQUE7SUFDQSw2QkFBQTtJQUNBLFlBQUE7SUFDQSxhQUFBO0VBa0ROO0VBakRNO0lBQ0ksZ0NBQUE7SUFDQSxZQUFBO0lBQ0Esa0JBQUE7SUFDQSxpQkFBQTtJQUNBLGdCQUFBO0lBQ0EsZUFBQTtJQUNBLGtCQUFBO0VBbURWO0VBbERNO0lBQ0ksYUFBQTtJQUNBLFlBQUE7RUFvRFY7RUFuRFU7SUFDSSxhQUFBO0VBcURkOztFQWpERTtJQUNJLGFBQUE7SUFDQSwyQkFBQTtJQUNBLFlBQUE7SUFDQSxtQkFBQTtJQUNBLDhCQUFBO0VBb0ROO0VBbkRNO0lBQ0ksWUFBQTtJQUNBLG9CQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0Esb0JDM1hRO0VEZ2JsQjtFQXBETTtJQUNJLGFBQUE7SUFDQSxZQUFBO0VBc0RWO0VBckRVO0lBQ0ksYUFBQTtFQXVEZDs7RUF0REU7SUFDSSxhQUFBO0lBQ0EsbUJBQUE7SUFDQSw2QkFBQTtJQUNBLGtCQUFBO0lBQ0EsYUFBQTtJQUNBLFlBQUE7RUF5RE47RUF4RE07SUFDSSxhQUFBO0lBQ0Esc0JBQUE7SUFDQSxZQUFBO0VBMERWO0VBekRVO0lBQ0ksa0JBQUE7SUFDQSwwQkFBQTtJQUFBLHVCQUFBO0lBQUEsa0JBQUE7SUFDQSxrQ0FBQTtJQUNBLGVBQUE7SUFDQSxpQkFBQTtFQTJEZDtFQTFEVTtJQUNJLG9CQ25aSTtJRG9aSixjQUFBO0lBQ0EsZ0JBQUE7RUE0RGQ7RUEzRE07SUFDSSxhQUFBO0lBQ0EsbUJBQUE7SUFDQSxtQkFBQTtFQTZEVjtFQTNEYztJQUNJLGVBQUE7RUE2RGxCOztFQTVERTtJQUNJLFlBQUE7SUFDQSxZQUFBO0lBQ0EsWUFBQTtJQUNBLG1CQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0EseUJBQUE7RUErRE47O0VBN0RFO0lBQ0ksMEJBQUE7SUFDQSwyQkFBQTtJQUNBLHdCQUFBO0lBQ0Esc0JBQUE7RUFnRU47O0VBcEVFO0lBQ0ksMEJBQUE7SUFDQSwyQkFBQTtJQUNBLHdCQUFBO0lBQ0Esc0JBQUE7RUFnRU47O0VBcEVFO0lBQ0ksMEJBQUE7SUFDQSwyQkFBQTtJQUNBLHdCQUFBO0lBQ0Esc0JBQUE7RUFnRU47O0VBOURFO0lBQ0ksYUFBQTtFQWlFTjs7RUEvREU7SUFDSSxpQkFBQTtFQWtFTjtFQWpFTTtJQUNJLFlBQUE7SUFDQSxZQUFBO0lBQ0EsWUFBQTtJQUNBLG1CQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0EsY0FBQTtJQUNBLHlCQUFBO0lBQ0EsZ0JBQUE7RUFtRVY7RUFsRU07SUFDSSx1QkFBQTtFQW9FVjtBQUNGIiwiZmlsZSI6ImNvbnRlbnRib2R5LmNvbXBvbmVudC5zYXNzIiwic291cmNlc0NvbnRlbnQiOlsiQHVzZSAnLi4vLi4vLi4vYXNzZXRzL3N0eWxlcy9mb250cydcblxuLyogUGhvbmUgKi9cbkBtZWRpYSBhbGwgYW5kICggbWF4LXdpZHRoOiA2OTlweCApXG4gICAgLmNvbnRlbnQtcGFuZWxcbiAgICAgICAgZGlzcGxheTogZmxleFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uXG4gICAgICAgIHBhZGRpbmc6IDIuNWVtXG4gICAgICAgIHBhZGRpbmctdG9wOiAwZW1cbiAgICAgICAgZmxleDogMVxuICAgICAgICBmbGV4LWZsb3c6IHdyYXBcbiAgICAgICAgI2pvdXJuZXktc3RhcnRcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXhcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW5cbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtZXZlbmx5XG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwZW1cbiAgICAgICAgICAgIGhlaWdodDogMzAwcHhcbiAgICAgICAgICAgIHBhZGRpbmc6IDFlbVxuICAgICAgICAgICAgZmxleDogMVxuICAgICAgICAgICAgLm1ha2UtYS1wYWdlLXRleHRcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4XG4gICAgICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtblxuICAgICAgICAgICAgICAgIHdpZHRoOiAyNDBweFxuICAgICAgICAgICAgICAgIGFsaWduLXNlbGY6IGNlbnRlclxuICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlclxuICAgICAgICAgICAgICAgICNqaWZmeVxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwZW1cbiAgICAgICAgICAgICAgICAgICAgZm9udC1mYW1pbHk6IGZvbnRzLiRwcmltYXJ5LWZvbnQtdHlwZSwgc2Fucy1zZXJpZlxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDI2cHhcbiAgICAgICAgICAgICAgICAuaW5mby10ZXh0XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBmb250cy4kcHJpbWFyeS1mb250LXR5cGUsIHNhbnMtc2VyaWZcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4XG4gICAgICAgICAgICAucGFnZUNyZWF0ZUZvcm1cbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4XG4gICAgICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtblxuICAgICAgICAgICAgICAgIGZsZXg6IDFcbiAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyXG4gICAgICAgICAgICAgICAgI2NyZWF0b3JQYWdlTmFtZVxuICAgICAgICAgICAgICAgICAgICAjc3RhcnRQYWdlQ3JlYXRlXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAyMHB4XG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXI6IG5vbmVcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDBweFxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDMwMHB4XG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHhcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA3MDBcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZThcbiAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlclxuICAgICAgICAgICAgICAgICNzdGFydFBhZ2VDcmVhdGlvblxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB4XG4gICAgICAgICAgICAgICAgICAgIC5idXR0b25cbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDBweFxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDMwMHB4XG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXI6IG5vbmVcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDIwcHhcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjNjg5MkQ1XG4gICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNzlEMUMzXG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNzAwXG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE2cHhcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IDAuM3NcbiAgICAgICAgLmNlbnRlci1ibG9ja1xuICAgICAgICAgICAgZGlzcGxheTogZmxleFxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtblxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXJcbiAgICAgICAgICAgIHBhZGRpbmc6IDFlbVxuICAgICAgICAgICAgaGVpZ2h0OiA0NDBweFxuICAgICAgICAgICAgd2lkdGg6IGluaGVyaXRcbiAgICAgICAgICAgIGZsZXg6IDFcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXJcbiAgICAgICAgICAgIC5pbmZvLXRleHQudGV4dC1sZWZ0XG4gICAgICAgICAgICAgICAgZm9udC1mYW1pbHk6IGZvbnRzLiRwcmltYXJ5LWZvbnQtdHlwZSwgc2Fucy1zZXJpZlxuICAgICAgICAgICAgICAgIHdpZHRoOiAyNTBweFxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDBcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE2cHhcbiAgICAgICAgICAgICAgICBwYWRkaW5nLXRvcDogNTBweFxuICAgICAgICAgICAgLm1hbi1wbGF5aW5nLWdhbWVzXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxNDBweFxuICAgICAgICAgICAgICAgIGltZ1xuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEyMHB4XG4gICAgICAgIC5zZWFyY2gtY3JlYXRvclxuICAgICAgICAgICAgZGlzcGxheTogZmxleFxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtblxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHlcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDBlbVxuICAgICAgICAgICAgaGVpZ2h0OiAyNzBweFxuICAgICAgICAgICAgcGFkZGluZzogMWVtXG4gICAgICAgICAgICBmbGV4OiAxXG4gICAgICAgICAgICAuY29udGVudC1oZWFkaW5nLnRleHQtbGVmdFxuICAgICAgICAgICAgICAgIGFsaWduLXNlbGY6IGNlbnRlclxuICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBmb250cy4kcHJpbWFyeS1mb250LXR5cGUsIHNhbnMtc2VyaWZcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4XG4gICAgICAgICAgICAuY3JlYXRvclNlYXJjaEZvcm1cbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4XG4gICAgICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtblxuICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXJcbiAgICAgICAgICAgICAgICBmbGV4OiAxXG4gICAgICAgICAgICAgICAgI2NyZWF0b3JcbiAgICAgICAgICAgICAgICAgICAgI2NyZWF0b3JGaW5kXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAyMHB4XG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXI6IG5vbmVcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDBweFxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDMwMHB4XG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHhcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA3MDBcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZThcbiAgICAgICAgICAgICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlclxuICAgICAgICAgICAgICAgICNjcmVhdG9yRmluZFN1Ym1pdFxuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nLXRvcDogMjBweFxuICAgICAgICAgICAgICAgICAgICAuYnV0dG9uXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDQwcHhcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAzMDBweFxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiBub25lXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAyMHB4XG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogIzY4OTJENVxuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzc5RDFDM1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDcwMFxuICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4XG4gICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiAwLjNzXG4gICAgICAgICN3b21hbi1zZWN0aW9uXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uLXJldmVyc2VcbiAgICAgICAgICAgIHBhZGRpbmc6IDBlbVxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXJcbiAgICAgICAgICAgIGZsZXgtZmxvdzogd3JhcC1yZXZlcnNlXG4gICAgICAgICAgICAuaW5mby10ZXh0LnRleHQtcmlnaHRcbiAgICAgICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwcHhcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE1cHhcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNTAwXG4gICAgICAgICAgICAgICAgZm9udC1mYW1pbHk6IGZvbnRzLiRwcmltYXJ5LWZvbnQtdHlwZSwgc2Fucy1zZXJpZlxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDFlbVxuXG4gICAgICAgICAgICAud29tYW4tb24tY29tcHV0ZXJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDEyMHB4XG4gICAgICAgICAgICAgICAgd2lkdGg6IDI2MHB4XG4gICAgICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDEwMHB4XG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTAwcHhcbiAgICAgICAgICAgICAgICBpbWdcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAyOTBweFxuXG4vKiBUYWJsZXQgKi9cbkBtZWRpYSBhbGwgYW5kICggbWluLXdpZHRoOiA3MDBweCApIGFuZCAoIG1heC13aWR0aDogOTU5cHggKVxuICAgIC5jb250ZW50LXBhbmVsXG4gICAgICAgIHBhZGRpbmc6IDNlbVxuICAgICAgICAjam91cm5leS1zdGFydFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93XG4gICAgICAgICAgICBmbGV4LXdyYXA6IHdyYXBcbiAgICAgICAgICAgIGZsZXg6IDFcbiAgICAgICAgICAgIGhlaWdodDogMjYwcHhcbiAgICAgICAgICAgIC5tYWtlLWEtcGFnZS10ZXh0XG4gICAgICAgICAgICAgICAgd2lkdGg6IDIwMHB4XG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleFxuICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW5cbiAgICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDNlbVxuICAgICAgICAgICAgICAgICNqaWZmeVxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDI2cHhcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMGVtXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBmb250cy4kcHJpbWFyeS1mb250LXR5cGUsIHNhbnMtc2VyaWZcbiAgICAgICAgICAgICAgICAuaW5mby10ZXh0XG4gICAgICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBmb250cy4kcHJpbWFyeS1mb250LXR5cGUsIHNhbnMtc2VyaWZcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4XG4gICAgICAgICAgICAucGFnZUNyZWF0ZUZvcm1cbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogZmxleFxuICAgICAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93XG4gICAgICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXJcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZy1yaWdodDogM2VtXG4gICAgICAgICAgICAgICAgICAgICNjcmVhdG9yUGFnZU5hbWVcbiAgICAgICAgICAgICAgICAgICAgICAgICNzdGFydFBhZ2VDcmVhdGVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDQwcHhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3JkZXI6IG5vbmVcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAyMHB4XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMHB4IDMwcHhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMjUwcHhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNzAwXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlOFxuICAgICAgICAgICAgICAgICAgICAjc3RhcnRQYWdlQ3JlYXRpb25cbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAyMHB4XG4gICAgICAgICAgICAgICAgICAgICAgICAuYnV0dG9uXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiBub25lXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA0MHB4XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDEwMHB4XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMjBweFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAjNjg5MkQ1XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzc5RDFDM1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA3MDBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE2cHhcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiAwLjNzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJjpob3ZlclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBib3gtc2hhZG93OiAzcHggM3B4IDNweFxuICAgICAgICAuY2VudGVyLWJsb2NrXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZFxuICAgICAgICAgICAgaGVpZ2h0OiBmaXQtY29udGVudFxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDFlbVxuICAgICAgICAgICAgLmluZm8tdGV4dC50ZXh0LWxlZnRcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDE2cHhcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMjVweFxuICAgICAgICAgICAgICAgIHdpZHRoOiAyMDBweFxuICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBmb250cy4kcHJpbWFyeS1mb250LXR5cGUsIHNhbnMtc2VyaWZcbiAgICAgICAgICAgICAgICBhbGlnbi1zZWxmOiBjZW50ZXJcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNTAwXG4gICAgICAgICAgICAubWFuLXBsYXlpbmctZ2FtZXNcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDI1MHB4XG4gICAgICAgICAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyXG4gICAgICAgICAgICAgICAgaW1nXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMjUwcHhcbiAgICAgICAgLnNlYXJjaC1jcmVhdG9yXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW5cbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXhcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3dcbiAgICAgICAgICAgIGZsZXgtd3JhcDogd3JhcFxuICAgICAgICAgICAgZmxleDogMVxuICAgICAgICAgICAgaGVpZ2h0OiAxNDBweFxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlclxuICAgICAgICAgICAgLmNvbnRlbnQtaGVhZGluZy50ZXh0LWxlZnRcbiAgICAgICAgICAgICAgICB3aWR0aDogMjAwcHhcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4XG4gICAgICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtblxuICAgICAgICAgICAgICAgIHBhZGRpbmctbGVmdDogM2VtXG4gICAgICAgICAgICAgICAgI3BhdHJvbi1jYWxsXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDBweFxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiA1cHhcbiAgICAgICAgICAgIC5jcmVhdG9yU2VhcmNoRm9ybVxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXhcbiAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93XG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlclxuICAgICAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDNlbVxuICAgICAgICAgICAgICAgIGhlaWdodDogZml0LWNvbnRlbnRcbiAgICAgICAgICAgICAgICAjY3JlYXRvclxuICAgICAgICAgICAgICAgICAgICAjY3JlYXRvckZpbmRcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDBweFxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiBub25lXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAyMHB4XG4gICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAwcHggMzBweFxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDI1MHB4XG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHhcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA3MDBcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZThcbiAgICAgICAgICAgICAgICAjY3JlYXRvckZpbmRTdWJtaXRcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDIwcHhcbiAgICAgICAgICAgICAgICAgICAgLmJ1dHRvblxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiBub25lXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDQwcHhcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDBweFxuICAgICAgICAgICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMjBweFxuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICM2ODkyRDVcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM3OUQxQzNcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA3MDBcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweFxuICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNpdGlvbjogMC4zc1xuICAgICAgICAgICAgICAgICAgICAgICAgJjpob3ZlclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJveC1zaGFkb3c6IDNweCAzcHggM3B4XG4gICAgICAgICN3b21hbi1zZWN0aW9uXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2VcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXhcbiAgICAgICAgICAgIHBhZGRpbmc6IDFlbVxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuXG4gICAgICAgICAgICAuaW5mby10ZXh0LnRleHQtcmlnaHRcbiAgICAgICAgICAgICAgICB3aWR0aDogMjIwcHhcbiAgICAgICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxZW1cbiAgICAgICAgICAgICAgICBmb250LWZhbWlseTogZm9udHMuJHByaW1hcnktZm9udC10eXBlLCBzYW5zLXNlcmlmXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4XG4gICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMFxuICAgICAgICAgICAgLndvbWFuLW9uLWNvbXB1dGVyXG4gICAgICAgICAgICAgICAgaW1nXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDUwcHhcblxuLyogRGVza3RvcCAqL1xuQG1lZGlhIGFsbCBhbmQgKCBtaW4td2lkdGg6IDk2MHB4IClcbiAgICAjcGFnZW5hbWUtaW5wdXRcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgdHJhbnNwYXJlbnRcbiAgICAgICAgZm9udC1mYW1pbHk6ICdncmFwaGlrIDQwMCcsc2Fucy1zZXJpZlxuICAgICAgICBmb250LXNpemU6IDI0cHhcbiAgICAgICAgbGluZS1oZWlnaHQ6IDY1cHhcbiAgICAgICAgY29sb3I6ICMwZDBjMjJcbiAgICAgICAgd2lkdGg6IDE1NXB4XG4gICAgICAgIGJhY2tncm91bmQ6IDAgMFxuICAgICAgICBjYXJldC1jb2xvcjogIzBkMGMyMlxuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxcHhcblxuICAgIC5jb250ZW50LXBhbmVsXG4gICAgICAgIGRpc3BsYXk6IGZsZXhcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtblxuICAgICAgICBwYWRkaW5nOiA0LjVlbVxuICAgICAgICBmbGV4OiAxIDBcbiAgICAgICAgLnNlYXJjaC1jcmVhdG9yXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93XG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seVxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogMGVtXG4gICAgICAgICAgICBoZWlnaHQ6IDEwMHB4XG4gICAgICAgICAgICBwYWRkaW5nOiAyZW1cbiAgICAgICAgICAgIC5jb250ZW50LWhlYWRpbmcudGV4dC1sZWZ0XG4gICAgICAgICAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyXG4gICAgICAgICAgICAgICAgZm9udC1mYW1pbHk6IGZvbnRzLiRwcmltYXJ5LWZvbnQtdHlwZSxzYW5zLXNlcmlmXG4gICAgICAgICAgICAgICAgI3BhdHJvbi1jYWxsXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDBweFxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHhcbiAgICAgICAgICAgIC5jcmVhdG9yU2VhcmNoRm9ybVxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXhcbiAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93XG4gICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlclxuICAgICAgICAgICAgICAgICNjcmVhdG9yXG4gICAgICAgICAgICAgICAgICAgIGlucHV0W3R5cGU9XCJ0ZXh0XCJdXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOjAgMzBweFxuXG5cblxuICAgICNjcmVhdG9yRmluZFxuICAgICAgICBoZWlnaHQ6IDUwcHhcbiAgICAgICAgd2lkdGg6IDQyMHB4XG4gICAgICAgIGJvcmRlcjogbm9uZVxuICAgICAgICBib3JkZXItcmFkaXVzOiAyMHB4XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweFxuICAgICAgICBmb250LXdlaWdodDogNzAwXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlOGU4ZThcblxuICAgICNjcmVhdG9yRmluZDo6cGxhY2Vob2xkZXJcbiAgICAgICAgZm9udC1zaXplOiAxNXB4ICFpbXBvcnRhbnRcbiAgICAgICAgZm9udC13ZWlnaHQ6IDcwMCAhaW1wb3J0YW50XG4gICAgICAgIG9wYWNpdHk6IDAuNDIgIWltcG9ydGFudFxuICAgICAgICBjb2xvcjogIzAwMCAhaW1wb3J0YW50XG5cbiAgICAjY3JlYXRvckZpbmQ6Zm9jdXNcbiAgICAgICAgb3V0bGluZTogbm9uZVxuXG4gICAgI2NyZWF0b3JGaW5kU3VibWl0XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAyMHB4XG4gICAgICAgIC5idXR0b25cbiAgICAgICAgICAgIGhlaWdodDogNTBweFxuICAgICAgICAgICAgd2lkdGg6IDE1MHB4XG4gICAgICAgICAgICBib3JkZXI6IG5vbmVcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDIwcHhcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjBweFxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDcwMFxuICAgICAgICAgICAgY29sb3I6ICM2ODkyRDVcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM3OUQxQzNcbiAgICAgICAgICAgIHRyYW5zaXRpb246IDAuM3NcbiAgICAgICAgLmJ1dHRvbjpob3ZlclxuICAgICAgICAgICAgYm94LXNoYWRvdzogM3B4IDNweCAzcHhcblxuXG4gICAgLmNlbnRlci1ibG9ja1xuICAgICAgICBkaXNwbGF5OiBmbGV4XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3dcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmRcbiAgICAgICAgcGFkZGluZzogMWVtXG4gICAgICAgIGhlaWdodDogNTAwcHhcbiAgICAgICAgLmluZm8tdGV4dC50ZXh0LWxlZnRcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBmb250cy4kcHJpbWFyeS1mb250LXR5cGUsIHNhbnMtc2VyaWZcbiAgICAgICAgICAgIHdpZHRoOiAyNjBweFxuICAgICAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMjVweFxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMFxuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4XG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDYwcHhcbiAgICAgICAgLm1hbi1wbGF5aW5nLWdhbWVzLnBsYWNlLXJpZ2h0XG4gICAgICAgICAgICBoZWlnaHQ6IDUwMHB4XG4gICAgICAgICAgICB3aWR0aDogNjAwcHhcbiAgICAgICAgICAgIGltZ1xuICAgICAgICAgICAgICAgIGhlaWdodDogNDUwcHhcblxuXG5cbiAgICAjd29tYW4tc2VjdGlvblxuICAgICAgICBkaXNwbGF5OiBmbGV4XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZVxuICAgICAgICBwYWRkaW5nOiAyZW1cbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW5cbiAgICAgICAgLmluZm8tdGV4dC50ZXh0LXJpZ2h0XG4gICAgICAgICAgICB3aWR0aDogMjIwcHhcbiAgICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDE0MHB4XG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHhcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDBcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBmb250cy4kcHJpbWFyeS1mb250LXR5cGVcbiAgICAgICAgLndvbWFuLW9uLWNvbXB1dGVyXG4gICAgICAgICAgICBoZWlnaHQ6IDcwMHB4XG4gICAgICAgICAgICB3aWR0aDogNzAwcHhcbiAgICAgICAgICAgIGltZ1xuICAgICAgICAgICAgICAgIGhlaWdodDogNzAwcHggXG4gICAgI2pvdXJuZXktc3RhcnRcbiAgICAgICAgZGlzcGxheTogZmxleFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtZXZlbmx5XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDBlbVxuICAgICAgICBoZWlnaHQ6IDEwMHB4XG4gICAgICAgIHBhZGRpbmc6IDJlbVxuICAgICAgICAubWFrZS1hLXBhZ2UtdGV4dFxuICAgICAgICAgICAgZGlzcGxheTogZmxleFxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtblxuICAgICAgICAgICAgd2lkdGg6IDIwMHB4XG4gICAgICAgICAgICAjamlmZnlcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHhcbiAgICAgICAgICAgICAgICB3aWR0aDogZml0LWNvbnRlbnRcbiAgICAgICAgICAgICAgICBmb250LWZhbWlseTogXCJwb3BwaW5zXCIsIHNhbnMtc2VyaWZcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHhcbiAgICAgICAgICAgICAgICBsaW5lLWhlaWdodDogMjVweFxuICAgICAgICAgICAgLmluZm8tdGV4dFxuICAgICAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBmb250cy4kcHJpbWFyeS1mb250LXR5cGVcbiAgICAgICAgICAgICAgICBjb2xvcjogI0E4QThBOFxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA3MDBcbiAgICAgICAgLnBhZ2VDcmVhdGVGb3JtXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4XG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93XG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyXG4gICAgICAgICAgICAjY3JlYXRvclBhZ2VOYW1lXG4gICAgICAgICAgICAgICAgaW5wdXRbdHlwZT1cInRleHRcIl1cbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzowIDMwcHhcbiAgICAjc3RhcnRQYWdlQ3JlYXRlXG4gICAgICAgIGhlaWdodDogNTBweFxuICAgICAgICB3aWR0aDogNDIwcHhcbiAgICAgICAgYm9yZGVyOiBub25lXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDIwcHhcbiAgICAgICAgZm9udC1zaXplOiAyMHB4XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA3MDBcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2U4ZThlOFxuXG4gICAgI3N0YXJ0UGFnZUNyZWF0ZTo6cGxhY2Vob2xkZXJcbiAgICAgICAgZm9udC1zaXplOiAxNXB4ICFpbXBvcnRhbnRcbiAgICAgICAgZm9udC13ZWlnaHQ6IDcwMCAhaW1wb3J0YW50XG4gICAgICAgIG9wYWNpdHk6IDAuNDIgIWltcG9ydGFudFxuICAgICAgICBjb2xvcjogIzAwMCAhaW1wb3J0YW50XG5cbiAgICAjc3RhcnRQYWdlQ3JlYXRlOmZvY3VzXG4gICAgICAgIG91dGxpbmU6IG5vbmVcblxuICAgICNzdGFydFBhZ2VDcmVhdGlvblxuICAgICAgICBtYXJnaW4tbGVmdDogMjBweFxuICAgICAgICAuYnV0dG9uXG4gICAgICAgICAgICBoZWlnaHQ6IDUwcHhcbiAgICAgICAgICAgIHdpZHRoOiAxNTBweFxuICAgICAgICAgICAgYm9yZGVyOiBub25lXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAyMHB4XG4gICAgICAgICAgICBmb250LXNpemU6IDE2cHhcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA3MDBcbiAgICAgICAgICAgIGNvbG9yOiAjNjg5MkQ1XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNzlEMUMzXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiAwLjNzXG4gICAgICAgIC5idXR0b246aG92ZXJcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDNweCAzcHggM3B4XG5cbiAgICAgICAgICAgIFxuXG5cblxuICAgICIsIi8vIEZvbnQgdHlwZXMvZmFtaWxpZXNcbiRwcmltYXJ5LWZvbnQtdHlwZTogcG9wcGluc1xuXG4vLyBGb250IHNpemVzXG4kdGl0bGUtZm9udC1zaXplOiA1NFxuJHN1YnRpdGxlLWZvbnQtc2l6ZTogMzZcbiRidXR0b24tdGV4dC1mb250LXNpemU6IDM2XG5cbi8vIEZvbnQgd2VpZ2h0c1xuJHRpdGxlLWZvbnQtd2VpZ2h0OiA3MDBcbiRidXR0b24tdGV4dC1mb250LXdlaWdodDogNzAwXG4kc3VidGl0bGUtZm9udC13ZWlnaHQ6IDYwMFxuJG5vcm1hbC10ZXh0LWZvbnQtd2VpZ2h0OiA1MDAiXX0= */"]
      });
      /***/
    },

    /***/
    7725:
    /*!*******************************************************************************!*\
      !*** ./src/app/components/login-overlay-form/login-overlay-form.component.ts ***!
      \*******************************************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "LoginOverlayFormComponent": function LoginOverlayFormComponent() {
          return (
            /* binding */
            _LoginOverlayFormComponent
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var angularx_social_login__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! angularx-social-login */
      7055);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      7716);
      /* harmony import */


      var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ng-bootstrap/ng-bootstrap */
      2664);
      /* harmony import */


      var _services_post_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ../../services/post.service */
      9166);
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      9895);

      var _LoginOverlayFormComponent = /*#__PURE__*/function () {
        function _LoginOverlayFormComponent(activeModal, authService, postService, router) {
          _classCallCheck(this, _LoginOverlayFormComponent);

          this.activeModal = activeModal;
          this.authService = authService;
          this.postService = postService;
          this.router = router;
          this.loggedIn = false;
          this.subscriptions = [];
        }

        _createClass(_LoginOverlayFormComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            console.log("ngonint is called");
            var subscription = this.authService.authState.subscribe(function (user) {
              _this.loggedIn = user != null;

              if (_this.loggedIn) {
                _this.postService.postGoogleAuth({
                  idToken: user.idToken
                }).subscribe(function (res) {
                  console.log(JSON.stringify(res));
                });
              }
            });
            this.subscriptions.push(subscription);
          }
        }, {
          key: "signInWithGoogle",
          value: function signInWithGoogle() {
            var _this2 = this;

            if (this.loggedIn === false) {
              this.authService.signIn(angularx_social_login__WEBPACK_IMPORTED_MODULE_1__.GoogleLoginProvider.PROVIDER_ID).then(function () {
                _this2.router.navigate(['newpage']);
              });
            }
          }
        }, {
          key: "SignInWithMetamask",
          value: function SignInWithMetamask() {
            console.log("Sign in with metamask");
          }
        }, {
          key: "signOut",
          value: function signOut() {
            this.authService.signOut();
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subscriptions.forEach(function (subscription) {
              return subscription.unsubscribe();
            });
          }
        }]);

        return _LoginOverlayFormComponent;
      }();

      _LoginOverlayFormComponent.ɵfac = function LoginOverlayFormComponent_Factory(t) {
        return new (t || _LoginOverlayFormComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__.NgbActiveModal), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](angularx_social_login__WEBPACK_IMPORTED_MODULE_1__.SocialAuthService), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_services_post_service__WEBPACK_IMPORTED_MODULE_0__.PostService), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__.Router));
      };

      _LoginOverlayFormComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
        type: _LoginOverlayFormComponent,
        selectors: [["app-login-overlay-form"]],
        decls: 17,
        vars: 0,
        consts: [[1, "modal-body"], [1, "row"], [1, "col-md-5", "login-graphic"], ["src", "../../../assets/images/login-woman.svg"], [1, "col-md-7", "login-form-container"], [1, "login-overlay-text"], ["type", "button", 1, "btn", "login-overlay-btn", 3, "click"], ["src", "../../../assets/images/google-icon.svg"], ["src", "../../../assets/images/metamask-icon.svg"]],
        template: function LoginOverlayFormComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](3, "img", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "h3", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](6, "Ready. Set. Koen.");

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](7, "button", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LoginOverlayFormComponent_Template_button_click_7_listener() {
              return ctx.signInWithGoogle();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](8, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](9, "img", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](10, " login with Google");

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](11, "button", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LoginOverlayFormComponent_Template_button_click_11_listener() {
              return ctx.activeModal.close("Sign in with metamask");
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](12, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](13, "img", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](14, " login with MetaMask");

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](15, "button", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function LoginOverlayFormComponent_Template_button_click_15_listener() {
              return ctx.signOut();
            })("click", function LoginOverlayFormComponent_Template_button_click_15_listener() {
              return ctx.activeModal.close("Logged out");
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](16, "Google Sign out (temp)");

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
          }
        },
        styles: [".modal-body {\n  padding: 0rem;\n}\n\n.modal-dialog {\n  margin: 0rem;\n  max-width: 800px;\n}\n\n.login-overlay .modal-content {\n  border-radius: 17px 17px 0 0;\n  background-color: #79d1c3;\n  position: absolute;\n  bottom: 0px;\n  transform: translateY(10%);\n}\n\n.login-graphic {\n  display: none;\n}\n\n.login-form-container {\n  padding: 6rem 3rem 7rem 3rem;\n  max-width: 400px;\n  margin: auto;\n}\n\n.login-overlay-input {\n  opacity: 0.7;\n  background-color: #F6F6F6;\n  border-radius: 5px;\n  padding: 1.2em;\n  max-width: 100%;\n  font-family: \"graphik 400\", sans-serif;\n}\n\n.login-overlay-text {\n  text-align: center;\n  font-size: 29.7;\n  margin-bottom: 20px;\n  color: #4d4d4d;\n  font-weight: 600;\n}\n\n.login-overlay-btn {\n  position: relative;\n  text-align: center;\n  border: 2px solid #57827F;\n  padding: 1.05em;\n  margin-top: 30px;\n  background-color: #e0f4f1;\n  width: 100%;\n  color: #57827F;\n  font-weight: 700;\n}\n\n.login-overlay-btn::before {\n  content: \" \";\n  position: absolute;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  left: 0;\n  box-shadow: 3px 3px 4px 0 rgba(0, 0, 0, 0.5);\n  opacity: 0;\n  transition: opacity 300ms;\n}\n\n.login-overlay-btn:hover {\n  background-color: #c5ebe5;\n  color: #57827F;\n}\n\n.login-overlay-btn:hover::before {\n  opacity: 1;\n}\n\n.login-overlay-btn img {\n  width: 6%;\n}\n\n@media all and (min-width: 360px) {\n  .login-overlay-btn {\n    text-align: left;\n  }\n  .login-overlay-btn span {\n    padding: 0 1.5em 0 1.2em;\n  }\n  .login-overlay-btn span img {\n    width: 12%;\n  }\n}\n\n@media all and (min-width: 768px) {\n  .login-overlay .modal-content {\n    border-radius: 17px;\n    background-color: #F6F6F6;\n    position: relative;\n    top: 50%;\n    transform: translateY(0%);\n  }\n\n  .modal-dialog {\n    max-width: 800px;\n    margin: 1.75em auto;\n  }\n\n  .login-graphic {\n    background-image: linear-gradient(#d4c0ff, #79d1c3);\n    border-radius: 17px 0 0 17px;\n    margin: 0rem;\n    margin-left: 15px;\n    max-width: 37%;\n    justify-content: center;\n    display: flex;\n  }\n  .login-graphic img {\n    width: 70%;\n  }\n\n  .login-overlay-text {\n    color: #000000;\n  }\n\n  .login-overlay-btn {\n    background-color: #F6F6F6;\n    color: #000000;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLW92ZXJsYXktZm9ybS5jb21wb25lbnQuc2FzcyIsIi4uLy4uLy4uL2Fzc2V0cy9zdHlsZXMvbGlnaHQtdGhlbWUtY29sb3JzLnNhc3MiLCIuLi8uLi8uLi9hc3NldHMvc3R5bGVzL2ZvbnRzLnNhc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBSUE7RUFDSSxhQUFBO0FBSEo7O0FBSUE7RUFDSSxZQUFBO0VBQ0EsZ0JBQUE7QUFESjs7QUFFQTtFQUNJLDRCQUFBO0VBQ0EseUJDVmtCO0VEV2xCLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLDBCQUFBO0FBQ0o7O0FBQ0E7RUFDSSxhQUFBO0FBRUo7O0FBQUE7RUFDSSw0QkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtBQUdKOztBQURBO0VBQ0ksWUFBQTtFQUNBLHlCQzFCcUI7RUQyQnJCLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxzQ0FBQTtBQUlKOztBQUZBO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JFMUJtQjtBRitCdkI7O0FBSEE7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxjQ3pDdUI7RUQwQ3ZCLGdCRXRDc0I7QUY0QzFCOztBQUpJO0VBQ0ksWUFBQTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUVBLDRDQUFBO0VBQ0EsVUFBQTtFQUNBLHlCQUFBO0FBS1I7O0FBSEk7RUFDSSx5QkFBQTtFQUNBLGNDMURtQjtBRCtEM0I7O0FBSEk7RUFDSSxVQUFBO0FBS1I7O0FBSEk7RUFDSSxTQUFBO0FBS1I7O0FBRkE7RUFDSTtJQUNJLGdCQUFBO0VBS047RUFKTTtJQUNJLHdCQUFBO0VBTVY7RUFMVTtJQUNJLFVBQUE7RUFPZDtBQUNGOztBQUxBO0VBQ0k7SUFDSSxtQkFBQTtJQUNBLHlCQ3JGaUI7SURzRmpCLGtCQUFBO0lBQ0EsUUFBQTtJQUNBLHlCQUFBO0VBT047O0VBTkU7SUFDSSxnQkFBQTtJQUNBLG1CQUFBO0VBU047O0VBUkU7SUFDSSxtREFBQTtJQUNBLDRCQUFBO0lBQ0EsWUFBQTtJQUNBLGlCQUFBO0lBQ0EsY0FBQTtJQUNBLHVCQUFBO0lBQ0EsYUFBQTtFQVdOO0VBVk07SUFDSSxVQUFBO0VBWVY7O0VBWEU7SUFDSSxjQ3BHbUI7RURrSHpCOztFQWJFO0lBQ0kseUJDekdpQjtJRDBHakIsY0N2R21CO0VEdUh6QjtBQUNGIiwiZmlsZSI6ImxvZ2luLW92ZXJsYXktZm9ybS5jb21wb25lbnQuc2FzcyIsInNvdXJjZXNDb250ZW50IjpbIkB1c2UgJy4uLy4uLy4uL2Fzc2V0cy9zdHlsZXMvbGlnaHQtdGhlbWUtY29sb3JzJ1xuQHVzZSAnLi4vLi4vLi4vYXNzZXRzL3N0eWxlcy9mb250cycgXG5cbi8vIHNtYWxsIHBob25lc1xuLm1vZGFsLWJvZHlcbiAgICBwYWRkaW5nOiAwcmVtXG4ubW9kYWwtZGlhbG9nXG4gICAgbWFyZ2luOiAwcmVtXG4gICAgbWF4LXdpZHRoOiA4MDBweFxuLmxvZ2luLW92ZXJsYXkgLm1vZGFsLWNvbnRlbnRcbiAgICBib3JkZXItcmFkaXVzOiAxN3B4IDE3cHggMCAwXG4gICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHQtdGhlbWUtY29sb3JzLiRsaWdodC1wcmltYXJ5LWNvbG9yXG4gICAgcG9zaXRpb246IGFic29sdXRlXG4gICAgYm90dG9tOjBweFxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgxMCUpXG5cbi5sb2dpbi1ncmFwaGljXG4gICAgZGlzcGxheTogbm9uZVxuXG4ubG9naW4tZm9ybS1jb250YWluZXJcbiAgICBwYWRkaW5nOiA2cmVtIDNyZW0gN3JlbSAzcmVtXG4gICAgbWF4LXdpZHRoOiA0MDBweFxuICAgIG1hcmdpbjogYXV0b1xuXG4ubG9naW4tb3ZlcmxheS1pbnB1dFxuICAgIG9wYWNpdHk6IDAuN1xuICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0LXRoZW1lLWNvbG9ycy4kbGlnaHQtYmFja2dyb3VuZC1jb2xvclxuICAgIGJvcmRlci1yYWRpdXM6IDVweFxuICAgIHBhZGRpbmc6IDEuMmVtXG4gICAgbWF4LXdpZHRoOiAxMDAlXG4gICAgZm9udC1mYW1pbHk6ICdncmFwaGlrIDQwMCcsc2Fucy1zZXJpZlxuXG4ubG9naW4tb3ZlcmxheS10ZXh0XG4gICAgdGV4dC1hbGlnbjogY2VudGVyXG4gICAgZm9udC1zaXplOiAyOS43XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweFxuICAgIGNvbG9yOiBsaWdodGVuKGxpZ2h0LXRoZW1lLWNvbG9ycy4kbGlnaHQtcHJpbWFyeS1mb250LWNvbG9yLCAzMCkgXG4gICAgZm9udC13ZWlnaHQ6IGZvbnRzLiRzdWJ0aXRsZS1mb250LXdlaWdodFxuXG4ubG9naW4tb3ZlcmxheS1idG5cbiAgICBwb3NpdGlvbjogcmVsYXRpdmVcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXJcbiAgICBib3JkZXI6IDJweCBzb2xpZCBsaWdodC10aGVtZS1jb2xvcnMuJGxpZ2h0LXByaW1hcnktZGFyay1jb2xvclxuICAgIHBhZGRpbmc6IDEuMDVlbVxuICAgIG1hcmdpbi10b3A6IDMwcHhcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGVuKGxpZ2h0LXRoZW1lLWNvbG9ycy4kbGlnaHQtcHJpbWFyeS1jb2xvciwgMjcpXG4gICAgd2lkdGg6IDEwMCVcbiAgICBjb2xvcjogbGlnaHQtdGhlbWUtY29sb3JzLiRsaWdodC1wcmltYXJ5LWRhcmstY29sb3JcbiAgICBmb250LXdlaWdodDogZm9udHMuJGJ1dHRvbi10ZXh0LWZvbnQtd2VpZ2h0XG5cbiAgICAmOjpiZWZvcmVcbiAgICAgICAgY29udGVudDogJyAnXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZVxuICAgICAgICB0b3A6IDBcbiAgICAgICAgcmlnaHQ6IDBcbiAgICAgICAgYm90dG9tOiAwXG4gICAgICAgIGxlZnQ6IDBcblxuICAgICAgICBib3gtc2hhZG93OiAzcHggM3B4IDRweCAwIHJnYmEoMCwgMCwgMCwgMC41KVxuICAgICAgICBvcGFjaXR5OiAwXG4gICAgICAgIHRyYW5zaXRpb246IG9wYWNpdHkgMzAwbXNcblxuICAgICY6aG92ZXJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRlbihsaWdodC10aGVtZS1jb2xvcnMuJGxpZ2h0LXByaW1hcnktY29sb3IsIDIwKVxuICAgICAgICBjb2xvcjogbGlnaHQtdGhlbWUtY29sb3JzLiRsaWdodC1wcmltYXJ5LWRhcmstY29sb3IgXG5cbiAgICAmOmhvdmVyOjpiZWZvcmVcbiAgICAgICAgb3BhY2l0eTogMVxuXG4gICAgaW1nXG4gICAgICAgIHdpZHRoOiA2JVxuICAgICAgICBcbi8vIG5vcm1hbCBhbmQgbGFyZ2UgcGhvbmVzXG5AbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiAzNjBweClcbiAgICAubG9naW4tb3ZlcmxheS1idG5cbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdFxuICAgICAgICBzcGFuXG4gICAgICAgICAgICBwYWRkaW5nOiAwIDEuNWVtIDAgMS4yZW1cbiAgICAgICAgICAgIGltZ1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxMiVcblxuLy8gdGFibGV0cyBhbmQgZGVza3RvcHNcbkBtZWRpYSBhbGwgYW5kIChtaW4td2lkdGg6IDc2OHB4KVxuICAgIC5sb2dpbi1vdmVybGF5IC5tb2RhbC1jb250ZW50XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE3cHhcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHQtdGhlbWUtY29sb3JzLiRsaWdodC1iYWNrZ3JvdW5kLWNvbG9yXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZVxuICAgICAgICB0b3A6NTAlIFxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoMCUpXG4gICAgLm1vZGFsLWRpYWxvZ1xuICAgICAgICBtYXgtd2lkdGg6IDgwMHB4XG4gICAgICAgIG1hcmdpbjogMS43NWVtIGF1dG9cbiAgICAubG9naW4tZ3JhcGhpY1xuICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoIGxpZ2h0LXRoZW1lLWNvbG9ycy4kbGlnaHQtdGVydGlhcnktY29sb3IsIGxpZ2h0LXRoZW1lLWNvbG9ycy4kbGlnaHQtcHJpbWFyeS1jb2xvciwpXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE3cHggMCAwIDE3cHhcbiAgICAgICAgbWFyZ2luOiAwcmVtXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxNXB4XG4gICAgICAgIG1heC13aWR0aDogMzclXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyXG4gICAgICAgIGRpc3BsYXk6IGZsZXhcbiAgICAgICAgaW1nXG4gICAgICAgICAgICB3aWR0aDogNzAlXG4gICAgLmxvZ2luLW92ZXJsYXktdGV4dFxuICAgICAgICBjb2xvcjogbGlnaHQtdGhlbWUtY29sb3JzLiRsaWdodC1wcmltYXJ5LWZvbnQtY29sb3JcbiAgICAubG9naW4tb3ZlcmxheS1idG5cbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHQtdGhlbWUtY29sb3JzLiRsaWdodC1iYWNrZ3JvdW5kLWNvbG9yIFxuICAgICAgICBjb2xvcjogbGlnaHQtdGhlbWUtY29sb3JzLiRsaWdodC1wcmltYXJ5LWZvbnQtY29sb3JcblxuICIsIiRsaWdodC1iYWNrZ3JvdW5kLWNvbG9yOiAjRjZGNkY2XG4kbGlnaHQtcHJpbWFyeS1jb2xvcjogIzc5ZDFjM1xuJGxpZ2h0LXNlY29uZGFyeS1jb2xvcjogIzY4OTJENVxuJGxpZ2h0LXByaW1hcnktZm9udC1jb2xvcjogIzAwMDAwMFxuJGxpZ2h0LXNlY29uZGFyeS1mb250LWNvbG9yOiAjOUQ5RDlEXG4kbGlnaHQtdGVydGlhcnktY29sb3I6ICNkNGMwZmZcbiRsaWdodC1wcmltYXJ5LWRhcmstY29sb3I6ICM1NzgyN0ZcbiIsIi8vIEZvbnQgdHlwZXMvZmFtaWxpZXNcbiRwcmltYXJ5LWZvbnQtdHlwZTogcG9wcGluc1xuXG4vLyBGb250IHNpemVzXG4kdGl0bGUtZm9udC1zaXplOiA1NFxuJHN1YnRpdGxlLWZvbnQtc2l6ZTogMzZcbiRidXR0b24tdGV4dC1mb250LXNpemU6IDM2XG5cbi8vIEZvbnQgd2VpZ2h0c1xuJHRpdGxlLWZvbnQtd2VpZ2h0OiA3MDBcbiRidXR0b24tdGV4dC1mb250LXdlaWdodDogNzAwXG4kc3VidGl0bGUtZm9udC13ZWlnaHQ6IDYwMFxuJG5vcm1hbC10ZXh0LWZvbnQtd2VpZ2h0OiA1MDAiXX0= */"],
        encapsulation: 2
      });
      /***/
    },

    /***/
    3252:
    /*!*******************************************************!*\
      !*** ./src/app/components/navbar/navbar.component.ts ***!
      \*******************************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "NavbarComponent": function NavbarComponent() {
          return (
            /* binding */
            _NavbarComponent
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @fortawesome/free-solid-svg-icons */
      9976);
      /* harmony import */


      var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ng-bootstrap/ng-bootstrap */
      2664);
      /* harmony import */


      var _login_overlay_form_login_overlay_form_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ../login-overlay-form/login-overlay-form.component */
      7725);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      7716);
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common */
      8583);
      /* harmony import */


      var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @fortawesome/angular-fontawesome */
      4163);

      function NavbarComponent_fa_icon_17_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "fa-icon", 14);
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("icon", ctx_r0.faBars);
        }
      }

      function NavbarComponent_fa_icon_18_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "fa-icon", 14);
        }

        if (rf & 2) {
          var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("icon", ctx_r1.faTimes);
        }
      }

      var _NavbarComponent = /*#__PURE__*/function () {
        function _NavbarComponent(modalService) {
          _classCallCheck(this, _NavbarComponent);

          this.modalService = modalService; //Menu Item icons

          this.faBars = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_2__.faBars;
          this.faTimes = _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_2__.faTimes; // Value chooses menu item icon

          this.onToggle = 1; // Classes used for mobile menu toggle

          this.isActive = false;
          this.isMenu = true; // Reason for closing modal

          this.closeResult = ''; //Toggle classes data structure

          this.toggleClasses = {};
        }

        _createClass(_NavbarComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.setCurrentMenuToggleClasses();
          }
          /*
           * initialize the toggle values
           */

        }, {
          key: "setCurrentMenuToggleClasses",
          value: function setCurrentMenuToggleClasses() {
            this.toggleClasses = {
              active: this.isActive,
              menu: this.isMenu
            };
          }
          /*
           * To toggle the menu for responsive design
           */

        }, {
          key: "toggleActive",
          value: function toggleActive() {
            this.isActive = !this.isActive;
            if (this.onToggle == 1) this.onToggle = 0;else this.onToggle = 1;
            this.setCurrentMenuToggleClasses();
          }
          /**
           * Run on click of buttons
           */

        }, {
          key: "onClick",
          value: function onClick(action) {
            console.log('You have clicked ' + action);
          }
        }, {
          key: "open",
          value: function open() {
            var _this3 = this;

            this.modalService.open(_login_overlay_form_login_overlay_form_component__WEBPACK_IMPORTED_MODULE_0__.LoginOverlayFormComponent, {
              centered: true,
              size: 'lg',
              modalDialogClass: 'login-overlay'
            }).result.then(function (result) {
              _this3.closeResult = "Closed with: ".concat(result);
            }, function (reason) {
              _this3.closeResult = "Dismissed ".concat(_this3.getDismissReason(reason));
            });
            console.log(this.closeResult);
          }
        }, {
          key: "getDismissReason",
          value: function getDismissReason(reason) {
            if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__.ModalDismissReasons.ESC) {
              return 'by pressing ESC';
            } else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__.ModalDismissReasons.BACKDROP_CLICK) {
              return 'by clicking on a backdrop';
            } else {
              return "with: ".concat(reason);
            }
          }
        }]);

        return _NavbarComponent;
      }();

      _NavbarComponent.ɵfac = function NavbarComponent_Factory(t) {
        return new (t || _NavbarComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__.NgbModal));
      };

      _NavbarComponent.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: _NavbarComponent,
        selectors: [["app-navbar"]],
        decls: 19,
        vars: 3,
        consts: [[1, "navbar"], [3, "ngClass"], [1, "logo"], [3, "(click"], ["src", "../../../assets/images/minimal-logo.png", "alt", "K\u014Den. Redirects you to home page"], [1, "item", "button"], [3, "click"], [1, "login-button"], [1, "login-text"], [1, "item", "button", "secondary"], [1, "signup-button"], [1, "signup-text"], [1, "toggle"], [3, "icon", 4, "ngIf"], [3, "icon"]],
        template: function NavbarComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "nav", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ul", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "li", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "a", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("(click", function NavbarComponent_Template_a__click_3_listener() {
              return ctx.onClick("home");
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "img", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "li", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "a", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function NavbarComponent_Template_a_click_6_listener() {
              return ctx.open();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "span", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, "Login");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "li", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "a", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function NavbarComponent_Template_a_click_11_listener() {
              return ctx.onClick("signup");
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "span", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14, "Sign Up");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "li", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "a", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function NavbarComponent_Template_a_click_16_listener() {
              return ctx.toggleActive();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](17, NavbarComponent_fa_icon_17_Template, 1, 1, "fa-icon", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](18, NavbarComponent_fa_icon_18_Template, 1, 1, "fa-icon", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", ctx.toggleClasses);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](16);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.onToggle === 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.onToggle === 0);
          }
        },
        directives: [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__.NgbNavbar, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgClass, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgIf, _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_5__.FaIconComponent],
        styles: ["nav[_ngcontent-%COMP%] {\n  background: #F6F6F6;\n}\n\na[_ngcontent-%COMP%] {\n  text-decoration: none;\n}\n\n.logo[_ngcontent-%COMP%] {\n  font-size: 20px;\n  padding: 7.5px 10px 7.5px 20px;\n}\n\n.item.button[_ngcontent-%COMP%] {\n  padding: 0.5em;\n}\n\n.item.button[_ngcontent-%COMP%]:hover {\n  transition: 0.3s;\n}\n\n.item[_ngcontent-%COMP%]:not(.button)   a[_ngcontent-%COMP%]:hover, .item[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover::after {\n  color: #6892D5;\n}\n\n.navbar[_ngcontent-%COMP%] {\n  display: block;\n}\n\n.menu[_ngcontent-%COMP%] {\n  list-style-type: none;\n  display: flex;\n  flex-wrap: wrap;\n  justify-content: space-between;\n  align-items: center;\n  padding: 0px;\n  padding-top: 20px;\n}\n\n.menu[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  display: block;\n  padding: 15px 5px;\n}\n\n.toggle[_ngcontent-%COMP%] {\n  padding-right: 20px;\n  order: 1;\n  font-size: 20px;\n}\n\n.toggle[_ngcontent-%COMP%]   fa-icon[_ngcontent-%COMP%] {\n  color: #6892D5;\n}\n\n.item.button[_ngcontent-%COMP%] {\n  order: 2;\n}\n\n.item[_ngcontent-%COMP%] {\n  width: 100%;\n  text-align: center;\n  display: none;\n  font-family: poppins, sans-serif;\n}\n\n.item[_ngcontent-%COMP%]   .login-button[_ngcontent-%COMP%]   .login-text[_ngcontent-%COMP%] {\n  color: #6892D5;\n  font-size: 20px;\n  font-weight: 700;\n}\n\n.item[_ngcontent-%COMP%]   .signup-button[_ngcontent-%COMP%]   .signup-text[_ngcontent-%COMP%] {\n  color: #6892D5;\n  font-size: 20px;\n  font-weight: 700;\n}\n\n.active[_ngcontent-%COMP%]   .item[_ngcontent-%COMP%] {\n  display: block;\n}\n\n.button[_ngcontent-%COMP%] {\n  background-color: #F6F6F6;\n  margin: 0 1.7em;\n}\n\n.button[_ngcontent-%COMP%]:hover {\n  background-color: #6892D5;\n  box-shadow: 0px 3.49144px 3.49144px;\n  border-radius: 20px;\n}\n\n.button[_ngcontent-%COMP%]:hover   .login-text[_ngcontent-%COMP%] {\n  color: #79d1c3 !important;\n}\n\n.button.secondary[_ngcontent-%COMP%] {\n  background-color: #79d1c3;\n  border-radius: 0.3rem;\n}\n\n.button.secondary[_ngcontent-%COMP%]:hover {\n  background-color: #6892D5;\n  box-shadow: 0px 3.49144px 3.49144px;\n}\n\n.button.secondary[_ngcontent-%COMP%]:hover   .signup-text[_ngcontent-%COMP%] {\n  color: #79d1c3;\n}\n\n.logo[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  height: 60px;\n}\n\n\n\n@media all and (min-width: 700px) {\n  .navbar[_ngcontent-%COMP%] {\n    display: block;\n  }\n\n  .menu[_ngcontent-%COMP%] {\n    justify-content: right;\n    padding-right: 2em;\n  }\n\n  .logo[_ngcontent-%COMP%] {\n    flex: 1;\n  }\n\n  .item.button[_ngcontent-%COMP%] {\n    width: auto;\n    order: 1;\n    display: block;\n  }\n\n  .toggle[_ngcontent-%COMP%] {\n    display: none;\n  }\n\n  \n  .menu[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n    padding: 10px 15px;\n  }\n\n  .button.secondary[_ngcontent-%COMP%] {\n    border: 0;\n  }\n\n  .button.secondary[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n    background: transparent;\n  }\n\n  .button[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n    text-decoration: none;\n  }\n}\n\n\n\n@media all and (min-width: 960px) {\n  .navbar[_ngcontent-%COMP%] {\n    display: block;\n  }\n\n  .menu[_ngcontent-%COMP%] {\n    align-items: flex-start;\n    flex-wrap: nowrap;\n    background: none;\n  }\n\n  .logo[_ngcontent-%COMP%] {\n    order: 0;\n  }\n\n  .item[_ngcontent-%COMP%] {\n    order: 1;\n    position: relative;\n    display: block;\n    width: auto;\n  }\n\n  .button[_ngcontent-%COMP%] {\n    order: 2;\n  }\n\n  .toggle[_ngcontent-%COMP%] {\n    display: none;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5hdmJhci5jb21wb25lbnQuc2FzcyIsIi4uLy4uLy4uL2Fzc2V0cy9zdHlsZXMvbGlnaHQtdGhlbWUtY29sb3JzLnNhc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBR0E7RUFDSSxtQkNKcUI7QURFekI7O0FBR0E7RUFDSSxxQkFBQTtBQUFKOztBQUNBO0VBQ0ksZUFBQTtFQUNBLDhCQUFBO0FBRUo7O0FBQUE7RUFDSSxjQUFBO0FBR0o7O0FBRkk7RUFDSSxnQkFBQTtBQUlSOztBQUhBOztFQUVJLGNDZm9CO0FEcUJ4Qjs7QUFIQTtFQUNJLGNBQUE7QUFNSjs7QUFMQTtFQUNJLHFCQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBUUo7O0FBUEE7RUFDSSxjQUFBO0VBQ0EsaUJBQUE7QUFVSjs7QUFUQTtFQUNJLG1CQUFBO0VBQ0EsUUFBQTtFQUNBLGVBQUE7QUFZSjs7QUFYSTtFQUNJLGNDcENnQjtBRGlEeEI7O0FBWkE7RUFDSSxRQUFBO0FBZUo7O0FBZEE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsZ0NBQUE7QUFpQko7O0FBZlE7RUFDSSxjQzlDWTtFRCtDWixlQUFBO0VBQ0EsZ0JBQUE7QUFpQlo7O0FBZlE7RUFDSSxjQ25EWTtFRG9EWixlQUFBO0VBQ0EsZ0JBQUE7QUFpQlo7O0FBZkE7RUFDSSxjQUFBO0FBa0JKOztBQWhCQTtFQUNJLHlCQzdEcUI7RUQ4RHJCLGVBQUE7QUFtQko7O0FBbEJJO0VBQ0kseUJDOURnQjtFRCtEaEIsbUNBQUE7RUFDQSxtQkFBQTtBQW9CUjs7QUFuQlE7RUFDSSx5QkFBQTtBQXFCWjs7QUFuQkE7RUFFSSx5QkN2RWtCO0VEd0VsQixxQkFBQTtBQXFCSjs7QUFwQkk7RUFDSSx5QkN6RWdCO0VEMEVoQixtQ0FBQTtBQXNCUjs7QUFyQlE7RUFDSSxjQzdFVTtBRG9HdEI7O0FBckJBO0VBQ0ksWUFBQTtBQXdCSjs7QUFyQkEsZ0JBQUE7O0FBQ0E7RUFDSTtJQUNJLGNBQUE7RUF3Qk47O0VBdkJFO0lBQ0ksc0JBQUE7SUFDQSxrQkFBQTtFQTBCTjs7RUF4QkU7SUFDSSxPQUFBO0VBMkJOOztFQXpCRTtJQUNJLFdBQUE7SUFDQSxRQUFBO0lBQ0EsY0FBQTtFQTRCTjs7RUExQkU7SUFDSSxhQUFBO0VBNkJOOztFQTNCRSxpQ0FBQTtFQUNBO0lBQ0ksa0JBQUE7RUE4Qk47O0VBeEJFO0lBQ0ksU0FBQTtFQTJCTjs7RUF6QkU7SUFDSSx1QkFBQTtFQTRCTjs7RUExQkU7SUFDSSxxQkFBQTtFQTZCTjtBQUNGOztBQTNCQSxpQkFBQTs7QUFDQTtFQUNJO0lBQ0ksY0FBQTtFQTZCTjs7RUE1QkU7SUFDSSx1QkFBQTtJQUNBLGlCQUFBO0lBQ0EsZ0JBQUE7RUErQk47O0VBN0JFO0lBQ0ksUUFBQTtFQWdDTjs7RUE5QkU7SUFDSSxRQUFBO0lBQ0Esa0JBQUE7SUFDQSxjQUFBO0lBQ0EsV0FBQTtFQWlDTjs7RUEvQkU7SUFDSSxRQUFBO0VBa0NOOztFQWhDRTtJQUNJLGFBQUE7RUFtQ047QUFDRiIsImZpbGUiOiJuYXZiYXIuY29tcG9uZW50LnNhc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAdXNlICcuLi8uLi8uLi9hc3NldHMvc3R5bGVzL2xpZ2h0LXRoZW1lLWNvbG9ycydcbkB1c2UgJy4uLy4uLy4uL2Fzc2V0cy9zdHlsZXMvZm9udHMnICAgICAgICBcbiAgICAgICAgXG5uYXZcbiAgICBiYWNrZ3JvdW5kOiBsaWdodC10aGVtZS1jb2xvcnMuJGxpZ2h0LWJhY2tncm91bmQtY29sb3JcbmFcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmVcbi5sb2dvXG4gICAgZm9udC1zaXplOiAyMHB4XG4gICAgcGFkZGluZzogNy41cHggMTBweCA3LjVweCAyMHB4XG5cbi5pdGVtLmJ1dHRvblxuICAgIHBhZGRpbmc6IDAuNWVtXG4gICAgJjpob3ZlclxuICAgICAgICB0cmFuc2l0aW9uOiAwLjNzXG4uaXRlbTpub3QoLmJ1dHRvbikgYTpob3ZlciAsXG4uaXRlbSBhOmhvdmVyOjphZnRlclxuICAgIGNvbG9yOiBsaWdodC10aGVtZS1jb2xvcnMuJGxpZ2h0LXNlY29uZGFyeS1jb2xvclxuXG4vLyBNb2JpbGUgTWVudVxuLm5hdmJhclxuICAgIGRpc3BsYXk6IGJsb2NrIFxuLm1lbnVcbiAgICBsaXN0LXN0eWxlLXR5cGU6IG5vbmVcbiAgICBkaXNwbGF5OiBmbGV4XG4gICAgZmxleC13cmFwOiB3cmFwXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlclxuICAgIHBhZGRpbmc6IDBweFxuICAgIHBhZGRpbmctdG9wOiAyMHB4XG4ubWVudSBsaSBhXG4gICAgZGlzcGxheTogYmxvY2tcbiAgICBwYWRkaW5nOiAxNXB4IDVweFxuLnRvZ2dsZVxuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHhcbiAgICBvcmRlcjogMVxuICAgIGZvbnQtc2l6ZTogMjBweFxuICAgIGZhLWljb25cbiAgICAgICAgY29sb3I6IGxpZ2h0LXRoZW1lLWNvbG9ycy4kbGlnaHQtc2Vjb25kYXJ5LWNvbG9yXG4uaXRlbS5idXR0b25cbiAgICBvcmRlcjogMlxuLml0ZW1cbiAgICB3aWR0aDogMTAwJVxuICAgIHRleHQtYWxpZ246IGNlbnRlclxuICAgIGRpc3BsYXk6IG5vbmVcbiAgICBmb250LWZhbWlseTogZm9udHMuJHByaW1hcnktZm9udC10eXBlLCBzYW5zLXNlcmlmXG4gICAgLmxvZ2luLWJ1dHRvblxuICAgICAgICAubG9naW4tdGV4dFxuICAgICAgICAgICAgY29sb3I6IGxpZ2h0LXRoZW1lLWNvbG9ycy4kbGlnaHQtc2Vjb25kYXJ5LWNvbG9yXG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHhcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA3MDBcbiAgICAuc2lnbnVwLWJ1dHRvblxuICAgICAgICAuc2lnbnVwLXRleHRcbiAgICAgICAgICAgIGNvbG9yOiBsaWdodC10aGVtZS1jb2xvcnMuJGxpZ2h0LXNlY29uZGFyeS1jb2xvclxuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4XG4gICAgICAgICAgICBmb250LXdlaWdodDogNzAwXG5cbi5hY3RpdmUgLml0ZW1cbiAgICBkaXNwbGF5OiBibG9ja1xuXG4uYnV0dG9uXG4gICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHQtdGhlbWUtY29sb3JzLiRsaWdodC1iYWNrZ3JvdW5kLWNvbG9yXG4gICAgbWFyZ2luOiAwIDEuN2VtXG4gICAgJjpob3ZlclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodC10aGVtZS1jb2xvcnMuJGxpZ2h0LXNlY29uZGFyeS1jb2xvclxuICAgICAgICBib3gtc2hhZG93OiAwcHggMy40OTE0NHB4IDMuNDkxNDRweFxuICAgICAgICBib3JkZXItcmFkaXVzOiAyMHB4XG4gICAgICAgIC5sb2dpbi10ZXh0XG4gICAgICAgICAgICBjb2xvcjogbGlnaHQtdGhlbWUtY29sb3JzLiRsaWdodC1wcmltYXJ5LWNvbG9yICFpbXBvcnRhbnRcblxuLmJ1dHRvbi5zZWNvbmRhcnlcbiAgICAvLyBtYXJnaW4tbGVmdDogMjBweFxuICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0LXRoZW1lLWNvbG9ycy4kbGlnaHQtcHJpbWFyeS1jb2xvclxuICAgIGJvcmRlci1yYWRpdXM6IDAuM3JlbVxuICAgICY6aG92ZXJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHQtdGhlbWUtY29sb3JzLiRsaWdodC1zZWNvbmRhcnktY29sb3JcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDMuNDkxNDRweCAzLjQ5MTQ0cHhcbiAgICAgICAgLnNpZ251cC10ZXh0XG4gICAgICAgICAgICBjb2xvcjogbGlnaHQtdGhlbWUtY29sb3JzLiRsaWdodC1wcmltYXJ5LWNvbG9yXG5cbi5sb2dvIGltZ1xuICAgIGhlaWdodDogNjBweFxuXG5cbi8qIFRhYmxldCBtZW51ICovXG5AbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiA3MDBweClcbiAgICAubmF2YmFyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrICBcbiAgICAubWVudSBcbiAgICAgICAganVzdGlmeS1jb250ZW50OiByaWdodFxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAyZW1cbiAgICBcbiAgICAubG9nbyBcbiAgICAgICAgZmxleDogMVxuICAgIFxuICAgIC5pdGVtLmJ1dHRvbiBcbiAgICAgICAgd2lkdGg6IGF1dG9cbiAgICAgICAgb3JkZXI6IDFcbiAgICAgICAgZGlzcGxheTogYmxvY2tcbiAgICBcbiAgICAudG9nZ2xlIFxuICAgICAgICBkaXNwbGF5OiBub25lXG4gICAgXG4gICAgLyogQnV0dG9uIHVwIGZyb20gdGFibGV0IHNjcmVlbiAqL1xuICAgIC5tZW51IGxpIGEgXG4gICAgICAgIHBhZGRpbmc6IDEwcHggMTVweFxuICAgICAgICAvLyBtYXJnaW46IDVweCAwXG4gICAgXG4gICAgLmJ1dHRvbiBhIFxuICAgICAgICAvLyBiYWNrZ3JvdW5kOiBsaWdodC10aGVtZS1jb2xvcnMuJGxpZ2h0LWJhY2tncm91bmQtY29sb3JcbiAgICBcbiAgICAuYnV0dG9uLnNlY29uZGFyeSBcbiAgICAgICAgYm9yZGVyOiAwXG4gICAgXG4gICAgLmJ1dHRvbi5zZWNvbmRhcnkgYSBcbiAgICAgICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnRcbiAgICBcbiAgICAuYnV0dG9uIGE6aG92ZXIgXG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZVxuICAgIFxuICAgIFxuLyogRGVza3RvcCBtZW51ICovXG5AbWVkaWEgYWxsIGFuZCAobWluLXdpZHRoOiA5NjBweClcbiAgICAubmF2YmFyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrIFxuICAgIC5tZW51IFxuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydCAgIFxuICAgICAgICBmbGV4LXdyYXA6IG5vd3JhcFxuICAgICAgICBiYWNrZ3JvdW5kOiBub25lXG4gICAgXG4gICAgLmxvZ28gXG4gICAgICAgIG9yZGVyOiAwXG4gICAgXG4gICAgLml0ZW0gXG4gICAgICAgIG9yZGVyOiAxXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZVxuICAgICAgICBkaXNwbGF5OiBibG9ja1xuICAgICAgICB3aWR0aDogYXV0b1xuICAgIFxuICAgIC5idXR0b24gXG4gICAgICAgIG9yZGVyOiAyXG4gICAgXG4gICAgLnRvZ2dsZSBcbiAgICAgICAgZGlzcGxheTogbm9uZSIsIiRsaWdodC1iYWNrZ3JvdW5kLWNvbG9yOiAjRjZGNkY2XG4kbGlnaHQtcHJpbWFyeS1jb2xvcjogIzc5ZDFjM1xuJGxpZ2h0LXNlY29uZGFyeS1jb2xvcjogIzY4OTJENVxuJGxpZ2h0LXByaW1hcnktZm9udC1jb2xvcjogIzAwMDAwMFxuJGxpZ2h0LXNlY29uZGFyeS1mb250LWNvbG9yOiAjOUQ5RDlEXG4kbGlnaHQtdGVydGlhcnktY29sb3I6ICNkNGMwZmZcbiRsaWdodC1wcmltYXJ5LWRhcmstY29sb3I6ICM1NzgyN0ZcbiJdfQ== */"]
      });
      /***/
    },

    /***/
    9166:
    /*!******************************************!*\
      !*** ./src/app/services/post.service.ts ***!
      \******************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "PostService": function PostService() {
          return (
            /* binding */
            _PostService
          );
        }
        /* harmony export */

      });
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common/http */
      1841);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      7716);

      var _PostService = /*#__PURE__*/function () {
        function _PostService(httpClient) {
          _classCallCheck(this, _PostService);

          this.httpClient = httpClient;
          this.googleAuth = window.origin + '/auth/google/jwt';
          this.url = '';
        }

        _createClass(_PostService, [{
          key: "get",
          value: function get() {
            return this.httpClient.get(this.url);
          }
        }, {
          key: "postGoogleAuth",
          value: function postGoogleAuth(post) {
            console.log("post request to backend");
            var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpHeaders().set('content-type', 'application/json');
            return this.httpClient.post(this.googleAuth, JSON.stringify(post), {
              headers: headers
            });
          }
        }]);

        return _PostService;
      }();

      _PostService.ɵfac = function PostService_Factory(t) {
        return new (t || _PostService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpClient));
      };

      _PostService.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
        token: _PostService,
        factory: _PostService.ɵfac,
        providedIn: 'root'
      });
      /***/
    },

    /***/
    2340:
    /*!*****************************************!*\
      !*** ./src/environments/environment.ts ***!
      \*****************************************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export */


      __webpack_require__.d(__webpack_exports__, {
        /* harmony export */
        "environment": function environment() {
          return (
            /* binding */
            _environment
          );
        }
        /* harmony export */

      }); // This file can be replaced during build by using the `fileReplacements` array.
      // `ng build` replaces `environment.ts` with `environment.prod.ts`.
      // The list of file replacements can be found in `angular.json`.


      var _environment = {
        production: false
      };
      /*
       * For easier debugging in development mode, you can import the following file
       * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
       *
       * This import should be commented out in production mode because it will have a negative impact
       * on performance if an error is thrown.
       */
      // import 'zone.js/plugins/zone-error';  // Included with Angular CLI.

      /***/
    },

    /***/
    4431:
    /*!*********************!*\
      !*** ./src/main.ts ***!
      \*********************/

    /***/
    function _(__unused_webpack_module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/platform-browser */
      9075);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      7716);
      /* harmony import */


      var _app_app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./app/app.module */
      6747);
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./environments/environment */
      2340);

      if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.production) {
        (0, _angular_core__WEBPACK_IMPORTED_MODULE_2__.enableProdMode)();
      }

      _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.platformBrowser().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_0__.AppModule)["catch"](function (err) {
        return console.error(err);
      });
      /***/

    }
  },
  /******/
  function (__webpack_require__) {
    // webpackRuntimeModules

    /******/
    "use strict";
    /******/

    /******/

    var __webpack_exec__ = function __webpack_exec__(moduleId) {
      return __webpack_require__(__webpack_require__.s = moduleId);
    };
    /******/


    __webpack_require__.O(0, ["vendor"], function () {
      return __webpack_exec__(4431);
    });
    /******/


    var __webpack_exports__ = __webpack_require__.O();
    /******/

  }]);
})();
//# sourceMappingURL=main-es5.js.map